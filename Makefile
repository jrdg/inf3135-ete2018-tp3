SRC_DIR = src
BIN_DIR = bin
BATS_FILE = test.bats
EXEC = tp3
TEST_EXEC = $(patsubst %.c,%,$(wildcard $(SRC_DIR)/test*.c))

.PHONY: exec bindir clean html source test testbats testbin testcunit

# 3. Copy the executable tp3 in the bin/ directory
exec: source bindir
	cp $(SRC_DIR)/tp3 $(BIN_DIR)

# 2. Create the bin/ directory
bindir:
	mkdir -p $(BIN_DIR)

# - Go into the src/ directory then execute make clean
# - Delete the bin/ directory 
# - Delete the README.html
clean:
	make clean -C $(SRC_DIR)
	rm -rf $(BIN_DIR)
	rm -f README.html

html:
	pandoc -f markdown -o README.html README.md -c misc/github-pandoc.css

# 1. Go into the src/ dir then execute make
source:
	make -C $(SRC_DIR)

test: exec testbin testcunit testbats

testbats:
	bats $(BATS_FILE)

testbin: source bindir
	$(MAKE) test -C $(SRC_DIR)
	cp $(TEST_EXEC) $(BIN_DIR)

testcunit:
	for test in `ls $(BIN_DIR)/test*` ; do \
		$$test; \
	done
