# Homework 3

## Description

This project has been done for the class of `Construction et maintenance de logiciels` sigle `3135` given by the professor `Alexandre Blondin Masse` @ablondin. The purpose of this project is to generate a file of a given format with some specifications.


## Authors

- Jordan Gauthier (GAUJ25089201)

## How it work 

This project is used for the purpose of generating files of a given format with some givens specifications. At this moment the program need to be executed by the `terminal` and need to have at least the programs listed in the [dependancy section](#dependancy) installed to work. The user need to execute the program with the wanted specifications like its showed in the exemples.

#### How to clone the project with https :

```bash
git clone --recurse-submodules -j8 https://gitlab.com/jrdg/inf3135-ete2018-tp3.git
```
*Since the project contain a submodule that itself contains the json file as long with the images you need to include the submodule when cloning the project.*

#### List of all the specifications that can be passed to the program :

```bash
Usage: bin/tp3 [--help] [--output-format FORMAT] [--output-filename FILENAME]
[--show-languages] [--show-capital] [--show-borders] [--show-flag]
[--country COUNTRY] [--region REGION]

Displays information about countries.

Optional arguments:
  --help                     Show this help message and exit.
  --output-format FORMAT     Selects the ouput format (either "text", "dot" or "png").
                             The "dot" format is the one recognized by Graphviz.
                             The default format is "text".
  --output-filename FILENAME The name of the output filename. This argument is
                             mandatory for the "png" format. For the "text" and "dot"
                             format, the result is printed on stdout if no output
                             filename is given.
  --show-languages           The official languages of each country are displayed.
  --show-capital             The capital of each country is displayed.
  --show-borders             The borders of each country are displayed.
  --show-flag                The flag of each country is displayed
                             (only for "dot" and "png" format).
  --country COUNTRY          The country code (e.g. "can", "usa") to be displayed.
  --region REGION            The region of the countries to be displayed.
                             The supported regions are "africa", "americas",
                             "asia", "europe" and "oceania".
```

#### Step by step usage :

1. Compile the project with the command `make`.
2. Execute the program `bin/tp3 [specifications....]`

#### Minimal exemple showing only one country with the TEXT format

The following command is entered :

```bash
bin/tp3 --show-capital --show-borders --show-languages --country can
```

*A filename can be specified with the option `--output-filename VALUE` to print the output into it instead of stdout.*

Then the output is printed to stdout :

```
------------------------------------------
    Name      : Canada
    Code      : CAN
    Capitals  :
        - Ottawa
    Languages :
        - English
        - French
    Borders   :
        - USA
------------------------------------------
```

#### Minimal exemple showing only one country with the DOT format

The following command is entered :

```bash
bin/tp3 --show-capital --show-borders --show-languages --show-flag --country can --output-format dot | neato -Goverlap=false -Tpng -o canada.png
```

*A filename can be specified with the option `--output-filename VALUE` to print the output into it instead of stdout.*

Then the output is printed to stdout :

```
graph {
    CAN [
        shape = none,
        label = <
            <table border="0" cellspacing="0">
                <tr>
                    <td align="center" border="1"><img src="countries/data/can.png" scale="true"/></td>
                </tr>
                <tr>
                    <td align="left" border="1"><b>Name : </b>Canada</td>
                </tr>
                <tr>
                    <td align="left" border="1"><b>Code : </b>CAN</td>
                </tr>
                <tr>
                    <td align="left" border="1"><b>Capitals : </b>Ottawa</td>
                </tr>
                <tr>
                    <td align="left" border="1"><b>Languages : </b>English, French</td>
                </tr>
                <tr>
                    <td align="left" border="1"><b>Borders : </b>USA</td>
                </tr>
            </table>
        >
    ];
}
```

Then the canada.png file is generated.

![Canada image](https://imagizer.imageshack.com/v2/448x328q90/r/923/O6J7t2.png "Canada.png")

If the arguments passed to the program regroup more than one country, the program will link every country that shares a border.

![Multiple country](https://imagizer.imageshack.com/v2/1415x890q90/r/924/it6OoO.png "random.png")

*To know more about a particular output format you can visit their respective merge request : [TEXT](https://gitlab.com/jrdg/inf3135-ete2018-tp3/merge_requests/6), [DOT](https://gitlab.com/jrdg/inf3135-ete2018-tp3/merge_requests/9) and [PNG](https://gitlab.com/jrdg/inf3135-ete2018-tp3/merge_requests/11)*

## Supported platforms

The project has been tested on the followings platforms :

- `MacOSX sierra V 10.13.1`

## Dependancy

The followings programs need to be installed to a proper usage of the program :

- `Graphviz` see [reference](https://www.graphviz.org)
- `Jansson` see [reference](https://jansson.readthedocs.io/en/2.11/)
- `cairo` see [reference](https://cairographics.org)
- `CUnit` see [reference](http://cunit.sourceforge.net)
- `bats` see [reference](https://github.com/sstephenson/bats)

## References

- Alexandre Blondin Masse @ablondin : 
    - `Makefile` 
    - The moqup of `parse_args` module

## Task

- [X] Parsing arguments (Jordan Gauthier)
- [X] Fetching json contents (Jordan Gauthier)
- [X] Generic list (Jordan Gauthier)
- [X] Output format TEXT (Jordan Gauthier)
- [X] Node module (Jordan Gauthier)
- [X] Output format DOT (Jordan Gauthier)
- [X] Output format PNG (Jordan Gauthier)
- [X] Refactoring X2 (Jordan Gauthier)
- [X] Units tests (Jordan Gauthier)
- [X] README file (Jordan Gauthier)

## State of the project

The project work like it should be. No bugs have been repertoried yet.

