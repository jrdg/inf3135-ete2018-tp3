#include "countries.h"

Countries * countries_init(){
    size_t init_capacity = 250;
    Countries * arr = malloc(sizeof(Countries));
    arr->values = malloc(sizeof(struct Country*) * init_capacity);
    arr->length = 0;
    arr->index = 0;
    arr->capacity = init_capacity;
    return arr;
}

unsigned int countries_append(
    Countries * arr,
    struct Country * country
){
    unsigned int insert = 1;
    struct Country ** realloc_tmp = NULL;
    if(arr->index == arr->capacity){
        realloc_tmp = realloc(arr->values, sizeof(struct Country*) * (arr->capacity * 2));
        if(realloc_tmp != NULL){
            arr->values = realloc_tmp;
            arr->capacity *= 2;
        } else insert = 0;
    }
    if(insert){
        arr->values[arr->index++] = country;
        arr->length++;
    }
    return insert;
}

void free_countries(Countries * arr){
    size_t i = 0;
    while(i < arr->length)
        free_country(arr->values[i++]);
    free(arr->values);
    free(arr);
}
