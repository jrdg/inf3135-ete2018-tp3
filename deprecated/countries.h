#ifndef COUNTRIES_H
#define COUNTRIES_H

#include <stdlib.h>
#include "strings.h"
#include "country.h"

typedef struct Countries {
    struct Country ** values;
    size_t capacity;
    size_t length;
    size_t index;
} Countries;

/**
 * Allocate the memory for a Countries struct.
 *
 * @return A pointer to the allocated Country
 */
Countries * countries_init();

/**
 * Append a country to countries
 *
 * @param countries the struct at which the country
 *        will be added.
 * @param country The country that ill be added to 
 *        countries. 
 */
unsigned int countries_append(
    Countries * countries,
    struct Country * country
);

/**
 * Free every country then free the countries array
 * then free the struct itself.
 *
 * @param The Countries to free
 */
void free_countries(Countries * countries);

#endif
