#include "link.h"

Link * link_init(char * pre , char * post) {
    Link * link = malloc(sizeof(Link));
    link->pre = malloc(sizeof(char) * (strlen(pre) + 1));
    link->post = malloc(sizeof(char) * (strlen(post) + 1));
    str_copy(pre, link->pre);
    str_copy(post, link->post);
    return link;
}

void print_link(Link * link , char * delim , FILE * pfile) {
    fprintf(pfile, "%s %s %s;\n", link->pre, delim, link->post);
}

void generic_free_links(void ** values, size_t length) {
    size_t i = 0;
    while(i < length)
        free_link((Link*)values[i++]);
}

void free_link(Link * link) {
    free(link->pre);
    free(link->post);
    free(link);
}
