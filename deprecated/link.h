
#include "stddef.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"

/**
 * A struct that represent a link in a graphviz file.
 *
 * @attribute pre the label that preced the delimiter
 * @attribute post the label that follow the delimiter.
 */
typedef struct Link {
    char * pre;
    char * post;
} Link;

/**
 * Init a link
 *
 * @pre pre must not be null
 * @pre post must not be null
 * @param pre the first label to add
 * @param post The seconde label
 * @return a A pointer to the link
 */
Link * link_init(char * pre , char * post);

/**
 * Print the specified link.
 *
 * @pre link must not be null
 * @pre delim must not be null
 * @pre pfile must not be null
 * @param link The link to print
 * @param delim The delimiter between the first and 
 *              second label.
 * @return pfile The stream in which print the link.
 */
void print_link(Link * link , char * delim , FILE * pfile);

/**
 * The function is used to free all the lements of type Link
 * in a Genlist.
 *
 * @param values An array of Link pointer.
 * @param length the length of that array
 */
void generic_free_links(void ** values, size_t length);

/**
 * Free the specified link
 *
 * @pre link must not be null
 * @param link the link to free
 */
void free_link(Link * link);
