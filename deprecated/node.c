/**
 * Implement the interface node.h
 *
 * @author Jordan Gauthier
 */

#include "node.h"


/**
 * Init and allocate a new node
 *
 * @param start The sequence that start the node
 * @param end The sequence that end the node
 * @return A pointer to the new node
 */
Node * node_init(char * start, char * post_start, char * end) {
    Node * node = malloc(sizeof(Node));
    node->kids = genlist_init(sizeof(Node*));
    node->attrs = genlist_init(sizeof(Attr*));
    node->start = malloc(sizeof(char) * (strlen(start) + 1));
    if(end != NULL) {
        node->end = malloc(sizeof(char) * (strlen(end) + 1));
        str_copy(end, node->end);
    }else node->end = NULL;
    if(post_start != NULL) {
        node->post_start = malloc(sizeof(char) * (strlen(post_start) + 1));
        str_copy(post_start, node->post_start);
    } else node->post_start = NULL;
    str_copy(start, node->start);
    return node;
}

void print_tree(Node * root, int tab) {
    if(root != NULL) {
        int next_tab = tab + TAB_INCREMENT;
        printf("%*s%s", tab,"",root->start);
        print_attrs(root->attrs);
        printf("%s\n",(root->post_start != NULL ? root->post_start : ""));
        for(size_t i = 0 ; i < root->kids->length ; i++) 
            print_tree((Node*)root->kids->values[i], next_tab);
        if(root->end != NULL) {
            printf("%*s%s\n",tab, "", root->end);
        }
    }
}

/**
 * This function need to be passed to free_genlist(). It
 * will free a Genlist of Nodes.
 *
 * @param values the Nodes
 * @param length The number of element in values.
 */
void generic_free_nodes(void ** values, size_t length) {
    size_t i = 0;
    while(i < length)
        free_node((Node*)values[i++]);
}

/**
 * Free the specified node
 */
void free_node(Node * node) {
    free(node->start);
    if(node->end != NULL)
        free(node->end);
    if(node->post_start != NULL)
        free(node->post_start);
    free_genlist(node->attrs, generic_free_attrs);
    free_genlist(node->kids, generic_free_nodes);
    free(node);
}
