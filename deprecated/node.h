/**
 * This file is the interface of the struct Node. It also contain the 
 * declarations of its functions. A Node is an element in a graphviz tree.
 *
 * @author Jordan Gauthier
 */
#ifndef NODE_H
#define NODE_H

#include <stddef.h>
#include <stdio.h>
#include "utils.h"
#include <stdlib.h>
#include "genlist.h"
#include "attr.h"

#define TAB_INCREMENT 4

/**
 * Represent a html node in a graphviz file
 *
 * @attribute start The sequence that start the node
 * @attribute post_start The sequence that end the starting sequence
 * @attribute attrs A list of attribute for the node
 * @attribute kids A list of childnodes
 * @attribute end The sequence that end the node
 */
typedef struct Node {
    char * start;
    char * post_start;
    char * end;
    Genlist * kids;
    Genlist * attrs;
} Node;

/**
 * Init and allocate a new node
 *
 * Exemple 1 :
 *
 *  Node * root = node_init("<td", ">", "</td>");
 *  print_tree(root,0);
 *
 *  output :
 *
 *  <td>
 *  </td>
 *
 *  Exemple 2 :
 *
 *  Node * root = node_init("graph {", NULL, "}");
 *  print_tree(root,0);
 *
 *  output:
 *
 *  graph {
 *  } 
 *      
 * @pre start must not be null
 * @pre end must not be null
 * @param start The sequence that start the node
 * @param post_start the sequence that end the starting sequence.
 * @param end The sequence that end the node
 * @return A pointer to the new node
 */
Node * node_init(char * start, char * post_start, char * end);

/**
 * Print all the node from the root
 *
 * @param the root
 */
void print_tree(Node * root, int tab);

/**
 * This function need to be passed to free_genlist(). It
 * will free a Genlist of Nodes.
 *
 * @param values the Nodes
 * @param length The number of element in values.
 */
void generic_free_nodes(void ** values, size_t length);

/**
 * Free the specified node
 */
void free_node(Node * node);

#endif
