#include "strings.h"


Strings * strings_init(){
    size_t init_capacity = 5;
    Strings * arr = malloc(sizeof(Strings));
    arr->values = malloc(sizeof(char*) * init_capacity);
    arr->length = 0;
    arr->index = 0;
    arr->capacity = init_capacity;
    return arr;
}

unsigned int append(Strings * arr , char * str){
    unsigned int insert = 1;
    char ** realloc_tmp = NULL;
    if(arr->index == arr->capacity){
        realloc_tmp = realloc(arr->values, sizeof(char*) * (arr->capacity*2));
        if(realloc_tmp != NULL){
            arr->values = realloc_tmp;
            arr->capacity *= 2;
        } else insert = 0;
    }
    if(insert){
        size_t len = strlen(str);
        arr->values[arr->index] = malloc(sizeof(char) * (len + 1));
        str_copy(str, (char*)arr->values[arr->index++]);
        arr->length++;
    }
    return insert;
}

void free_strings(Strings * arr){
    size_t i = 0;
    while(i < arr->length)
        free(arr->values[i++]);
    free(arr->values);
    free(arr);
}
