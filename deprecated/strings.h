#ifndef STRINGS_H
#define STRINGS_H

#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include "utils.h"

typedef struct Strings {
    char ** values;  // The Strings array
    size_t index;          // The next insertion index in values.
    size_t length;         // The current number of element of values
    size_t capacity;       // The current maximum capacity of values.
} Strings;


/**
 * Allocate necessary amount of space for a Strings
 *
 * @return A pointer to the new allocated Strings
 */
Strings * strings_init();

/**
 * Append str to the specified Strings. str do
 * not need to be previously allocated because
 * malloc is used in this function then str copied to 
 * it.
 *
 * @param arr The Strings struct
 * @param str The String that we want to add to arr
 * @return 1 if str have been added otherwise it return 0
 */
unsigned int append(Strings * arr , char * str);

/**
 * Free all the string in arr then free arr itself.
 */
void free_strings(Strings * arr);

#endif
