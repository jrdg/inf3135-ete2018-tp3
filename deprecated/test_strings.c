/**
 * Testing the `strings` module with CUnit.
 *
 * @author Jordan gauthier
 */
#include "strings.h"
#include "CUnit/Basic.h"

size_t get_capacity(size_t nb){
    size_t start = 5;
    while(nb > start) start *= 2;
    return start;
}

void test_strings_append(){
    Strings * arr = strings_init();
    size_t i = 0;
    size_t length = 99;
    while(i++ < length) append(arr, "test");
    CU_ASSERT_EQUAL(arr->capacity, get_capacity(arr->length));
    CU_ASSERT_EQUAL(arr->length,   length);
    CU_ASSERT_EQUAL(arr->index,    length)
}

int main() {
    CU_pSuite pSuite = NULL;
    if (CU_initialize_registry() != CUE_SUCCESS )
        return CU_get_error();

    // Testing arguments parsing
    pSuite = CU_add_suite("Testing Strings", NULL, NULL);
    if (pSuite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking append no error",
                    test_strings_append) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }




    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}

