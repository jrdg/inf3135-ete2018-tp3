/**
 * Implement the interface attr.h
 *
 * @author Jordan Gauthier
 * @code permanent GAUJ25089201
 */

#include "attr.h"

// ------ //
// Public //
// ------ //

Attr * attr_init(
    char const * const name,
    char const * const value
) {
    Attr * attr = malloc(sizeof(Attr));
    attr->name = 
        malloc(sizeof(char) * (strlen(name) + 1));
    attr->value = 
        malloc(sizeof(char) * (strlen(value) + 1));
    str_copy(name, attr->name);
    str_copy(value, attr->value);
    return attr;
}

void print_attrs(
    Genlist const * const attrs,
    FILE * const pfile
) {
    Attr * attr = NULL;
    for(size_t i = 0 ; i < attrs->length ; i++) {
        attr = (Attr*)attrs->values[i];
        fprintf(pfile, " %s=\"%s\"",
                attr->name, attr->value);
    } 
}

void generic_free_attrs(
    void ** values,
    size_t length
) {
    size_t i = 0;
    while(i < length)
        free_attr((Attr*)values[i++]);
}

void free_attr(
    Attr * attr
) {
    free(attr->name);
    free(attr->value);
    free(attr);
}
