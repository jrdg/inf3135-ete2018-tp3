/**
 * This is the interface of the struct Attr. It also contain the declarations
 * of its functions. An Attr is an attribute to be added to a Node.
 *
 * @author Jordan Gauthier
 * @code permanent GAUJ25089201
 */

#ifndef ATTR_H
#define ATTR_H

#include <stddef.h>
#include <stdio.h>
#include "utils.h"
#include <stdlib.h>
#include "genlist.h"

/**
 * Represent an attribute for a node
 *
 * @attribute name The name of the attributes
 * @attribute value The value of the attribute
 */
typedef struct Attr {
    char * name;
    char * value;
} Attr;

/**
 * Init and allocate a new Attr
 *
 * @pre name must not be null
 * @pre value must not be null
 * @param name The name of the Attr
 * @param value The value of the Attr
 * @return a pointer to the newly allocated Attr
 */
Attr * attr_init(
    char const * const name,
    char const * const value
);

/**
 * Print a list of attributes
 *
 * @pre attrs must not be null
 * @pre pfile must not be null
 * @param attrs The attirbutes list
 * @param pfile The stream in which the Attrs
 *              will be printed
 */
void print_attrs(
    Genlist const * const attrs,
    FILE * const pfile
);

/**
 * This function need to be passed to free_genlist(). It
 * will free a Genlist of Attrs.
 *
 * @pre values must not be null
 * @param values the Attrs
 * @param length The number of element in values.
 */
void generic_free_attrs(
    void ** values,
    size_t length
);

/**
 * Free the specified Attr
 *
 * @pre attr must not be null
 * @param attr The Attr to free
 */
void free_attr(
    Attr * attr
);

#endif
