/**
 * Implement the interface country.h
 *
 * @author Jordan Gauthier
 * @code permanent GAUJ25089201
 */

#include "country.h"

// ------- //
// Private //
// ------- //

/**
 * This function is used to free the content of each of the
 * string into evey Genlist<String>
 *
 * @param values The array of pointer of which we want to free
 * @param length the length values
 */
void free_country_genlist(
    void ** values, size_t length
){
    size_t i = 0;
    while(i < length)
        free(values[i++]);
}

// ------ //
// Public //
// ------ //

struct Country * country_init(){
    struct Country * country = 
        malloc(sizeof(struct Country));
    country->name = NULL;
    country->code = NULL;
    country->capitals = genlist_init(sizeof(char*));
    country->region = NULL;
    country->borders = genlist_init(sizeof(char*));
    country->languages = genlist_init(sizeof(char*));
    country->img_url = NULL;
    return country;
}

void copy_country(
    struct Country const * const source,
    struct Country * const target
) {
    size_t i = 0;
    if(source->name != NULL)
        set_country_name(target, source->name);
    if(source->code != NULL)
        set_country_code(target, source->code);
    if(source->img_url != NULL)
        set_country_img(target, source->img_url);
    while(i < source->capitals->length)
        add_capital(target, 
                source->capitals->values[i++]);
    i = 0;
    while(i < source->languages->length)
        add_language(target, 
                source->languages->values[i++]);
    i = 0;
    while(i < source->borders->length)
        add_border(target, 
                source->borders->values[i++]);
}

void generic_free_countries(
    void ** values,
    size_t length
) {
    size_t i = 0;
    while(i < length){
        free_country((struct Country*)values[i++]);
    }
}

void set_country_name(
    struct Country * const country,
    char const * const name
){
    size_t len = strlen(name);
    country->name = malloc(sizeof(char) * (len + 1));
    str_copy(name, country->name);
}

void set_country_code(
    struct Country * const country,
    char const * const code
){
    size_t len = strlen(code);
    country->code = malloc(sizeof(char) * (len + 1));
    str_copy(code, country->code);
}

void set_country_img(
    struct Country * const country,
    char const * const img
){
    size_t len = strlen(img);
    country->img_url = malloc(sizeof(char) * (len + 1));
    str_copy(img, country->img_url);
}

unsigned int add_capital(
    struct Country * const country,
    char const * const capital
) {
    size_t len = strlen(capital);
    if(len > 0){
        char * newcap = malloc(sizeof(char) * (len + 1));
        str_copy(capital,newcap);
        return genlist_append(country->capitals,newcap);
    }
    return 0;
}

unsigned int add_language(
    struct Country * const country,
    char const * const lang
) {
    size_t len = strlen(lang);
    if(len > 0){
        char * newlang = malloc(sizeof(char) * (len + 1));
        str_copy(lang,newlang);
        return genlist_append(country->languages,newlang);
    }
    return 0;
}

unsigned int add_border(
    struct Country * const country,
    char const * const border
) {
    size_t len = strlen(border);
    if(len > 0) {
        char * newborder = 
            malloc(sizeof(char) * (strlen(border) + 1));
        str_copy(border,newborder);
        return genlist_append(country->borders,newborder);
    }
    return 0;
}

void print_country(
    struct Country const * const country,
    struct Arguments const * const args,
    FILE * const pfile
) {
    size_t i = 0;
    fprintf(pfile ,"------------------------------------------\n");
    fprintf(pfile, "    Name      : %s\n",country->name);
    fprintf(pfile, "    Code      : %s\n",country->code);
    if(country->capitals->length > 0) {
       fprintf(pfile, "    Capitals  : \n");
        while(i < country->capitals->length)
            fprintf(pfile, "        - %s\n",
                    (char*)country->capitals->values[i++]);
    }
    if(country->languages->length > 0) {
        fprintf(pfile, "    Languages : \n");
        i = 0;
        while(i < country->languages->length)
            fprintf(pfile, "        - %s\n",
                    (char*)country->languages->values[i++]);
    }
    if(args->show_borders && country->borders->length > 0) {
        fprintf(pfile, "    Borders   : \n");
        i = 0;
        while(i < country->borders->length)
            fprintf(pfile, "        - %s\n",
                    (char*)country->borders->values[i++]);
    }
}

void free_country(
    struct Country * country
) {
    free(country->name);
    free(country->code);
    free(country->region);
    free(country->img_url);
    free_genlist(country->capitals,free_country_genlist);
    free_genlist(country->borders, free_country_genlist);
    free_genlist(country->languages, free_country_genlist);
    free(country);
}
