/**
 * Declaration of an interface representing a single country
 *
 * @author Jordan Gauthier
 * @code permanent GAUJ25089201
 */

#ifndef COUNTRY_H
#define COUNTRY_H

#include <jansson.h>
#include <stdlib.h>
#include "genlist.h"
#include <string.h>
#include "utils.h"
#include "parse_args.h"

struct Country {
    char * name;
    char * code;
    Genlist * capitals;
    char * region;
    Genlist * borders;
    Genlist * languages;
    char * img_url;
};

/**
 * Allocate the memory for a Country struct.
 *
 * @return A pointer to the allocated Country
 */
struct Country * country_init();

/**
 * Copy a all the values of a country in another country. It does copy the
 * values of the  capirals, languages and borders and NOT the adresses.
 *
 * @pre source must not be null
 * @pre target must not be null
 * @param source The country fromwhich the values will be copied
 * @param target The country in which the valuess will be copied.
 */
void copy_country(
    struct Country const * const source,
    struct Country * const target
);

/**
 * Set the name of the specified country
 *
 * @pre country must not be null
 * @pre name must not be null
 * @param country the struct country
 * @param name the name to affect
 */
void set_country_name(
    struct Country * const country,
    char const * const name
);

/**
 * Set the code of the specified country
 *
 * @pre country must not be null
 * @pre code mut not be null
 * @param country the struct country
 * @param code the code to affect
 */
void set_country_code(
    struct Country * const country,
    char const * const code
);

/**
 * Set the img  of the specified country
 *
 * @pre country must not be null
 * @pre img must not be null
 * @param country the struct country
 * @param img the img to affect
 */

void set_country_img(
    struct Country * const country,
    char const * const img
);

/**
 * Add a capital to the capitals array of the specified country
 *
 * @pre country must not be null
 * @pre capital must not be null
 * @post check whether the result return 1 or not.
 * @param country The country to which the capital will be added
 * @param capital A string representation of the capital that will
 *        be added to country
 * @return 1 If the capital has been added.
 */
unsigned int add_capital(
    struct Country * const country,
    char const * const capital
);

/**
 * Add a language to the languages array of the specified country
 *
 * @pre country must not be null
 * @pre lang must not be nulli
 * @post check whether the result return 1 or not.
 * @param country The country to which the language will be added
 * @param lang A string representation of the language that will
 *        be added to country
 * @return 1 If the language has been added.
 */
unsigned int add_language(
    struct Country * const country,
    char const * const lang
);

/**
 * Add a border to the borders array of the specified country
 *
 * @pre country must not be null
 * @pre border must not be null
 * @post check whether the result return 1 or not.
 * @param country The country to which the border will be added.
 * @param border A string representation of the border that will
 *        be added to country
 * @return 1 If the border has been added.
 */
unsigned int add_border(
    struct Country * const country,
    char const * const border
);


/**
 * print the specified country
 *
 * @pre country must not be null
 * @param country The country to print
 */
void print_country(
    struct Country const * const country, 
    struct Arguments const * const args,
    FILE * const pfile
);

/**
 * This function is to be affected to the parameter of the
 * function pointer of the function free_genlist. The purpose
 * of it is to free every country in a genlist
 *
 * @pre values must not be null
 * @param values An array of pointer
 * @param length The length of values
 */
void generic_free_countries(
    void ** values,
    size_t length
);

/**
 * Free every string in country then free the Strings
 * then free country itself.
 *
 * @pre country must not be null
 * @param country The country to free
 */
void free_country(
    struct Country * country
);

#endif
