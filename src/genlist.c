/**
 * Implement the interface genlist.h
 *
 * @author Jordan gauthier
 * @code permanent GAUJ25089201
 */

#include "genlist.h"

Genlist * genlist_init(
    size_t size_one_alloc
) {
    size_t init_capacity = 5;
    Genlist * arr = malloc(sizeof(Genlist));
    arr->values = malloc(size_one_alloc * init_capacity);
    arr->length = 0;
    arr->index = 0;
    arr->capacity = init_capacity;
    arr->size_one_alloc = size_one_alloc;
    return arr;
}

unsigned int genlist_append(
    Genlist * arr,
    void * const newvalue
){
    unsigned int insert = 1;
    void ** realloc_tmp = NULL;
    if(arr->index == arr->capacity){
        realloc_tmp = realloc(arr->values, arr->size_one_alloc * (arr->capacity * 2));
        if(realloc_tmp != NULL){
            arr->values = realloc_tmp;
            arr->capacity *= 2;
        } else insert = 0;
    }
    if(insert){
        arr->values[arr->index++] = newvalue;
        arr->length++;
    }
    return insert;
}


void free_genlist(
    Genlist * arr, 
    void(*free_content)(void**,size_t)
) {
    if(free_content != NULL)
        free_content(arr->values,arr->length);
    free(arr->values);
    free(arr);
}


