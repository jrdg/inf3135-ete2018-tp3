/**
 * Declaration of an interface representing a generic list
 * that can contain a ~infinite amount of data of a single type
 *
 * @author Jordan gauthier
 * @code permanent GAUJ25089201
 */

#ifndef GENLIST_H
#define GENLIST_H

#include <stdlib.h>
#include <stddef.h>

/**
 * The struct Genlist
 *
 * @attribute values The array that contain all the data.
 * @attribute capacity The actual capacity of the array.
 * @attribute lenght The number of elements in the array.
 * @attribute index The index at which the next element 
 *                  will be isnerted.
 * @attribute size_one_alloc The size of one element.
 */
typedef struct  Genlist {
    void ** values;
    size_t capacity;
    size_t length;
    size_t index;
    size_t size_one_alloc;
} Genlist;

/**
 * ALlocated the necessary memory for a Genlist of a given type
 *
 * @param size_one_alloc The size of one element
 * @return A pointer to the newly allocated Genlist
 */
Genlist * genlist_init(
    size_t size_one_alloc
);

/**
 * Add an element to the specified Genlist.
 *
 * @param arr The Genlist in which the element will be inserted
 * @param newvalu The value that will be inserted
 * @return 1 if the insertion is succesful 0 otherwise
 */
unsigned int genlist_append(
    Genlist * arr,
    void * const newvalue
);

/**
 * Free the Genlist with all its elements so you don't have
 * to free each element in the list even if they were all dynamicaly
 * allocated.
 *
 * @param arr The Genlist to free
 * @param free_content A pointer of function that define how to
 *                     free the elements in the Genlist.
 */
void free_genlist(
    Genlist * arr, 
    void(*free_content)(void**, size_t)
);

#endif
