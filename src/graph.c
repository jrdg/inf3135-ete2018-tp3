/**
 * Implement the interface graph.h
 *
 * @author Jordan gauthier
 * @code permanenant Jordan Gauthier
 */

#include "graph.h"

// ------- //
// Private //
// ------- //

/**
 * Create a Node of type LINK.
 *
 * @paeam first_tag The first tag of the link
 * @param second_tag The second tag of the link
 * @return A pointer to the newly allocated Node
 */
Node * get_link_node(
    char const * const first_tag,
    char const * const second_tag
) {
    Node * node = node_init(
                LINK,
                UNSPEC_BRACES_TYPE,
                UNSPEC_LINE,
                first_tag
           );
    node_set_content(node, second_tag);
    return node;
}

/**
 * Create a Node of type ROOT.
 *
 * @paeam tag The tag of the node
 * @return A pointer to the newly allocated Node.
 */
Node * get_root_node(
    char const * const tag
) {
    return node_init(
                ROOT,
                UNSPEC_BRACES_TYPE,
                NEW_LINE,
                tag
           );
}

/**
 * Create a Node of type GLOBAL
 *
 * @param value The tag of the node
 * @return A pointer to the newly allocated Node
 */
Node * get_global_node(
    char const * const tag
) {
    return node_init(
                GLOBAL,
                UNSPEC_BRACES_TYPE,
                NEW_LINE,
                tag
           );
}

/**
 * Create a Node of type OPTION with a content
 *
 * @param option The tag of the option
 * @param value The value of the option
 * @return A pointer to the newly allocated Node.
 */
Node * get_option_content_node(
    char const * const tag,
    char const * const value
) {
    Node * node = node_init(
                       OPTION,
                       UNSPEC_BRACES_TYPE,
                       UNSPEC_LINE,
                       tag
                  );
    node_set_content(node, value);
    return node;
}

/**
 * Create a Node of type OPTION without a content
 *
 * @param tag The tag of the option
 * @return A pointer to the newly allocated Node.
 */
Node * get_option_nl_node(
    char const * const tag
) {
    Node * node = node_init(
                       OPTION,
                       UNSPEC_BRACES_TYPE,
                       NEW_LINE,
                       tag
                  );
    return node;
}

/**
 * Create a Node of type HTML_TEXT
 *
 * @param txt The text that the node will contain.
 * @return A pointer to the newly allocated Node
 */
Node * get_text_node(
    char const * const txt
) {
    return node_init(
                HTML_TEXT,
                UNSPEC_BRACES_TYPE,
                UNSPEC_LINE,
                txt
           );
}


/**
 * Create a Node of type HTML, of line type SAME_LINE
 * and of braces type DOUBLE
 *
 * @param tag The teag that the node will contain.
 * @return A pointer to the newly allocated Node
 */
Node * get_html_sl_double_node(
    char const * const tag
) {
    return node_init(
                HTML, 
                DOUBLE,
                SAME_LINE,
                tag
           );
}

/**
 * Create a Node of type HTML, of line type NEW_LINE
 * ans of braces type DOUBLE
 *
 * @param txt The text that the node will contain.
 * @return A pointer to the newly allocated Node
 */
Node * get_html_nl_double_node(
    char const * const tag
) {
    return node_init(
                HTML, 
                DOUBLE,
                NEW_LINE,
                tag
           );
}

/**
 * Create a Node of type HTML, of line type NEW_LINE
 * ans of braces type SINGLE
 *
 * @param txt The text that the node will contain.
 * @return A pointer to the newly allocated Node
 */
Node * get_html_nl_single_node(
    char const * const tag
) {
    return node_init(
                HTML, 
                SINGLE,
                NEW_LINE,
                tag
           );
}

/**
 * Create a Node of type HTML, of line type SAME_LINE
 * ans of braces type SINGLE
 *
 * @param txt The text that the node will contain.
 * @return A pointer to the newly allocated Node
 */
Node * get_html_sl_single_node(
    char const * const tag
) {
    return node_init(
                HTML, 
                SINGLE,
                SAME_LINE,
                tag
           );
}

/**
 * Test a a country code is in the countries that we want.
 *
 * @pre countries must not be null
 * @pre searched_code must not be null
 * @pre low must be equal or bigger than 0
 * @pre high must be smaller than the length of countries
 * @param low the index from which the search start.
 * @param high The index from which the search end
 * @return the index of the country of the specified country
 *         code or -1 if the country code has not been found.
 */
int bsearch_country(
    Genlist const * const countries ,
    char const * const searched_code ,
    int low,
    int high
) {
    if(low > high)
        return -1;
    int mid = (low + high ) / 2;
    struct Country * country = 
        (struct Country*)countries->values[mid];
    signed int result = 
        str_compare(searched_code, country->code);
    if(result == 0) 
        return mid;
    else if(result > 0)
        return bsearch_country(countries,
                searched_code, mid + 1, high);
    else 
        return bsearch_country(countries,
                searched_code, low, mid - 1);
}

/**
 * Sort the specified countries alphabeticaly from their country code.
 *
 * @pre countries must not be null
 * @pre start must be >= 0 (most of the time it should be 0)
 * @pre end This value must be smaller than the length of countries
 * @param countries the array of country
 * @param start the index from which the sort start.
 * @param end the index from which sort end.
 */
void quicksort_code(
    Genlist const * const countries,
    int start,
    int end
) {
    if(start < end) {
        int i = start + 1;
        int j = end;
        struct Country ** c =
            (struct Country**)countries->values;
        struct Country * pivot = c[start];
        struct Country * c_tmp = NULL;
        while(j >= i) {
            if(str_compare(c[i]->code, pivot->code) > 0 &&
               str_compare(c[j]->code, pivot->code) <= 0) {
                c_tmp = c[i];
                c[i] = c[j];
                c[j] = c_tmp;
            }
            if(str_compare(c[i]->code, pivot->code) <= 0) i++;
            if(str_compare(c[j]->code, pivot->code) > 0) j--;
        }
        c[start] = c[j];
        c[j] = pivot;
        quicksort_code(countries,start,j-1);
        quicksort_code(countries,j+1,end);
   }
}

/**
 * Sort the specified counties alphabeticaly from their
 * country code.
 *
 * @pre countries must not bel null
 * @param countries the array of countries.
 */
void insertion_sort(
    Genlist const * const countries
) {
    struct Country * key = NULL;
    struct Country * cj = NULL;
    size_t j = 0;
    for(size_t i = 1 ; i < countries->length ; i++){
        key = (struct Country*)countries->values[i];
        j = i - 1;
        cj = (struct Country*)countries->values[j];
        while((int)j > -1 &&
              str_compare(cj->code, key->code) > 0) {
            countries->values[j + 1] = cj;
            cj = (struct Country*)countries->values[--j];
        }
        countries->values[((int)j) + 1] = key;
    }
}

/**
 *
 * Iterate throught the borders of the iterated country
 * and add every valid link to links.
 *
 * THE COUNTRIES MUST BE SORTED ALPHABETICALY FROM THEIR
 * COUNTRY CODE AND THIS FUNCTION MUST BE CALL IN A LOOP
 * IN WHICH THE COUNTRIES ARE ITERATED ONE BY ONE
 *
 * VALID LINK : A link is added when the followings conditions 
 *              are both true. 1. The border must be in the countries.
 *              2. The iterated border of the iterated country is
 *              alphabeticaly bigger than the iterated country.
 *
 * Exemple :
 *          C = {CAN = {USA} , MEX = {USA} , USA = {CAN,MEX}}
 *          Iterated country = USA
 *          USA > CAN = No link added (The link already exist)
 *          USA > MEX = No link added (The link already exist)
 *
 * @pre link must not be null
 * @pre countries must not be null
 * @param links The list in which every link will be added
 * @param countries The list of all the fetched countries
 * @param i The index of the current iterated country
 */
void check_links(
    Genlist * const links,
    Genlist const * const countries,
    size_t i
) {
    Node * link = NULL;
    char * c_sub = NULL;
    int index = 0;
    struct Country * c_main = 
        (struct Country*)countries->values[i];
    for(size_t j = 0 ; j < c_main->borders->length ; j++) {
        c_sub = (char*)c_main->borders->values[j];
        if((index = bsearch_country(countries,
           c_sub,0,countries->length-1)) >= 0 && 
            index > (int)i) {
            link = get_link_node(c_main->code, c_sub);
            genlist_append(links, link);
        }
    }
}

/**
 * Set the attributes of a table cell (Node)
 *
 * @pre table must not be null.
 * @param table The Node that represent the table cell.
 */
void set_table_attrs(
    Node * const table
) {
    Attr * attr = NULL;
    char * attrs[] = {"border", "cellspacing"};
    char * values[] = {"0", "0"};
    size_t len = 2;
    for(size_t i = 0 ; i < len ; i++) {
        attr = attr_init(attrs[i], values[i]);
        node_add_attr(table,attr);
    }
}

/**
 * Set the attributes of a td cell (Node) that hold 
 * an img cell (Node).
 *
 * @pre td must not be null
 * @param td The Node that represent the td cell
 */
void set_td_img_attrs(
    Node * const td
) {
    Attr * attr = NULL;
    char * attrs[] = {"align", "border"};
    char * values[] = {"center", "1"};
    size_t len = 2;
    for(size_t i = 0 ; i < len ; i++) {
        attr = attr_init(attrs[i], values[i]);
        node_add_attr(td, attr);
    }
}

/**
 * Set the attributes of a td cell (Node)
 *
 * @pre td must not be null
 * @param td The Node that represent the td
 */
void set_td_attrs(
    Node * const td
) {
    Attr * attr = NULL;
    char * attrs[] = {"align", "border"};
    char * values[] = {"left", "1"};
    size_t len = 2;
    for(size_t i = 0 ; i < len ; i++) {
        attr = attr_init(attrs[i], values[i]);
        node_add_attr(td, attr);
    }
}

/**
 * Set the attributes of an img cell (Node)
 *
 * @pre img must not be null
 * @param img The Node that represent the img
 * @param src The img_url
 */
void set_img_attrs(
    Node * const img,
    char * const src
) {
    Attr * attr = NULL;
    char * attrs[] = {"src", "scale"};
    char * values[] = {src, "true"};
    size_t len = 2;
    for(size_t i = 0 ; i < len ; i++) {
        attr = attr_init(attrs[i], values[i]);
        node_add_attr(img, attr);
    }
}

/**
 * Create 4 Nodes that together will formed a line in
 * a html table. This line represent the country code.
 *
 * @pre country must not be null
 * @param country The country in which the the images is in.
 * @return A pointer to the newly allocated Node. 
 */
Node * get_node_img(
    struct Country const * const country
) {
    Node * tr = NULL;
    if(country->img_url != NULL) {
        tr = get_html_nl_double_node("tr");
        Node * td = get_html_sl_double_node("td");
        Node * img = get_html_sl_single_node("img");
        set_td_img_attrs(td);
        set_img_attrs(img, country->img_url);
        node_add_child(td, img);
        node_add_child(tr, td);
    }
    return tr;
}

/**
 * Create 4 Nodes that together will formed a line in
 * a html table. This line represent the capitals.
 *
 * @pre country must not be null
 * @param country The country in which the capitals are in.
 * @return A pointer to the newly allocated Node. 
 */
Node * get_node_capitals(
    struct Country const * const country
) {
    Node * tr = NULL;
    if(country->capitals->length > 0) {
        Node * title_tag = get_html_sl_double_node("b");
        Node * title = get_text_node("Capitals : ");
        char * newstring = 
            app_genlist_tstr("", country->capitals, 0);
        Node * title_value = get_text_node(newstring);
        tr = get_html_nl_double_node("tr");
        Node * capitals = get_html_sl_double_node("td");
        free(newstring);
        set_td_attrs(capitals);
        node_add_child(title_tag, title);
        node_add_child(capitals, title_tag);
        node_add_child(capitals, title_value);
        node_add_child(tr, capitals);
    }
    return tr;
}

/**
 * Create 4 Nodes that together will formed a line in
 * a html table. This line represent the languages.
 *
 * @pre country must not be null
 * @param country The country in which the languages are in.
 * @return A pointer to the newly allocated Node. 
 */
Node * get_node_languages(
    struct Country const * const country
) {
    Node * tr = NULL;
    if(country->languages->length > 0) {
        Node * title_tag = get_html_sl_double_node("b");
        Node * title = get_text_node("Languages : ");
        char * newstring = 
            app_genlist_tstr("", country->languages, 0);
        Node * title_value = get_text_node(newstring);
        tr = get_html_nl_double_node("tr");
        Node * languages = get_html_sl_double_node("td");
        free(newstring);
        set_td_attrs(languages);
        node_add_child(title_tag, title);
        node_add_child(languages, title_tag);
        node_add_child(languages, title_value);
        node_add_child(tr, languages);
    }
    return tr;
}

/**
 * Create 4 Nodes that together will formed a line in
 * a html table. This line represent the borders.
 *
 * @pre country must not be null
 * @param country The country in which the borders are in.
 * @return A pointer to the newly allocated Node. 
 */
Node * get_node_borders(
    struct Country const * const country,
    struct Arguments const * const args
) {
    Node * tr = NULL;
    if(args->show_borders && 
       country->borders->length > 0) {
        Node * title_tag = get_html_sl_double_node("b");
        Node * title = get_text_node("Borders : ");
        char * newstring = 
            app_genlist_tstr("", country->borders, 0);
        Node * title_value = get_text_node(newstring);
        tr = get_html_nl_double_node("tr");
        Node * borders = get_html_sl_double_node("td");
        free(newstring);
        set_td_attrs(borders);
        node_add_child(title_tag, title);
        node_add_child(borders, title_tag);
        node_add_child(borders, title_value);
        node_add_child(tr, borders);
    }
    return tr;
}



/**
 * Create 4 Nodes that together will formed a line in
 * a html table. This line represent the country name.
 *
 * @pre country must not be null
 * @param country The country in which the name is in.
 * @return A pointer to the newly allocated Node. 
 */
Node * get_node_name(
    struct Country const * const country
) {
    Node * tr = NULL;
    if(country->name != NULL) {
        Node * title_tag = get_html_sl_double_node("b");
        Node * title = get_text_node("Name : ");
        Node * title_value = get_text_node(country->name);
        tr = get_html_nl_double_node("tr");
        Node * name = get_html_sl_double_node("td");
        set_td_attrs(name);
        node_add_child(title_tag, title);
        node_add_child(name, title_tag);
        node_add_child(name, title_value);
        node_add_child(tr, name);
    }
    return tr;
}

/**
 * Create 4 Nodes that together will formed a line in
 * a html table. This line represent the country code.
 *
 * @pre country must not be null
 * @param country The country in which the code is in.
 * @return A pointer to the newly allocated Node. 
 */
Node * get_node_code(
    struct Country const * const country
) {
    Node * tr = NULL;
    if(country->code != NULL) { 
        Node * title_tag = get_html_sl_double_node("b");
        Node * title = get_text_node("Code : ");
        Node * title_value = get_text_node(country->code);
        tr = get_html_nl_double_node("tr");
        Node * code = get_html_sl_double_node("td");
        set_td_attrs(code);
        node_add_child(title_tag, title);
        node_add_child(code, title_tag);
        node_add_child(code, title_value);
        node_add_child(tr, code);
    }
    return tr;
}


/**
 * Create all the needed Nodes specified in the country and
 * in the args then add them to the table.
 *
 * @pre table must not be null
 * @pre country must not be null
 * @pre args must not be null
 * @param table The Node in which all the created Nodes
 *              will be added.
 * @param country The country in which the information that
 *                will be put in the Nodes are in.
 * @param args The arguments in which the specifications
 *             are in.
 */
void check_country_values(
    Node * const table,
    struct Country const * const country,
    struct Arguments const * const args
) {
    Node * img = get_node_img(country);
    Node * name  = get_node_name(country);
    Node * code = get_node_code(country);
    Node * capitals = get_node_capitals(country);
    Node * languages = get_node_languages(country);
    Node * borders = get_node_borders(country, args);
    if(img != NULL)
        node_add_child(table, img);
    node_add_child(table, name);
    node_add_child(table, code);
    if(capitals != NULL)
        node_add_child(table, capitals);
    if(languages != NULL)
        node_add_child(table, languages);
    if(borders != NULL)
        node_add_child(table, borders);
}

/**
 * Iterate through the countries then foreach of the iterated
 * country create a Node with all the needed childnodes specified
 * in the Arguments structure.
 *
 * @pre graph must not be null
 * @pre countries must not be null
 * @pre args must not be null
 * @param graph The graph in which the root node is in.
 * @param countries The countries
 * @param args The argument in which the specification are in.
 */
void init_countries_nodes(
    Graph * const graph,
    Genlist const * const countries,
    struct Arguments const * const args
) {
    Genlist * links = genlist_init(sizeof(Node*));
    Node * country_node = NULL;
    Node * table = NULL;
    Node * shape = NULL;
    Node * label = NULL;
    struct Country ** cnts = 
        (struct Country**)countries->values;
    for(size_t i = 0 ; i < countries->length ; i++) {
        shape = get_option_content_node("shape","none");
        label = get_option_nl_node("label");
        country_node = get_global_node(cnts[i]->code);
        table = get_html_nl_double_node("table");
        check_country_values(table, cnts[i], args);
        check_links(links , countries, i);
        set_table_attrs(table);
        node_add_child(label, table);
        node_add_child(country_node, shape);
        node_add_child(country_node, label);
        node_add_child(graph->root, country_node);
    }
    for(size_t i = 0 ; i < links->length ; i++)
        node_add_child(graph->root, links->values[i]);
    /**
     * The memory of the links genlist is not free using the usual function
     * free_genlist because if we do so, it will also free the node in it and
     * we dont want that because the adresse of each nodes is copied to the
     * genlist of the childnodes of ROOT so we still need them (The node in LINKS)
     */
    free(links->values);
    free(links);
}

// ------ //
// Public //
// ------ //


Graph * graph_init(
    Genlist const * const countries,
    struct Arguments const * const args
){
    Graph * graph = malloc(sizeof(Graph));
    graph->root = get_root_node("graph");
    /*
     * The countries array is nearly sorted
     * so its better to use insertion than
     * an o(log n) alternative.
     * */
    insertion_sort(countries);
    init_countries_nodes(graph, countries, args);
    return graph;
}

void print_graph(
    Graph const * const graph,
    FILE * pfile
) {
    node_print_tree(NULL, graph->root, 0, pfile);
    fputc('\n', pfile);
}

void free_graph(
    Graph * graph
) {
    free_node(graph->root);
    free(graph);
}
