/**
 * This file represent an interface that use the module Node
 * to build a pre defined Node tree for the purpose of generate
 * a .dot file then using it with graphviz.
 *
 * @author Jordan Gauthier
 * @code permanent GAUJ25089201
 */

#ifndef GRAPH_H
#define GRAPH_H

#include "genlist.h"
#include <stddef.h>
#include "country.h"
#include "utils.h"
#include "node.h"
#include "parse_args.h"

/**
 * The struct that represent the graph
 */
typedef struct Graph {
    Node * root;
} Graph;

/**
 * Init a graph with the specifed countries.
 *
 * @pre countries must not be null
 * @param countries The countries from which the graph 
 *        will be build.
 * @return A pointer to the new allocated graph
 */
Graph * graph_init(
    Genlist const * const countries,
    struct Arguments const * const args
);

/**
 * Print the specified graph to the specified steam
 *
 * @pre graph must not be null
 * @pre pfile must not be null
 * @param graph the graph to print
 * @param pfile the stream in which print the result
 */
void print_graph(
    Graph const * const graph,
    FILE * pfile
);

/**
 * Free the specified graph form memory
 *
 * @pre graph must not be null
 * @pre graph must has been allocated
 * @param graph The graph to free
 */
void free_graph(
    Graph * graph
);

#endif
