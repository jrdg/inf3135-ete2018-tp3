/**
 * Implementation of the interface json_worker.h
 *
 * @author Jordan Gauthier
 * @code permanent GAUJ25089201
 */

#include "json_worker.h"

#define COUNTRY_NB 250
#define JSON_COUNTRIES_PATH "countries/countries.json"
#define JSON_IMG_PATH "countries/data/"
#define JSON_COUNTRY_CODE "cca3"
#define JSON_COUNTRY_NAME "name"
#define JSON_COUNTRY_CAPITALS "capital"
#define JSON_COUNTRY_BORDERS "borders"
#define JSON_COUNTRY_LANGS "languages"
#define JSON_COMMON_NAME "common"
#define JSON_REGION "region"

// ------- //
// Private //
// ------- //

/**
 * Get the name of the country then affect to country
 *
 * @param country The country to which the name will be affected.
 * @param obj A json single country
 */
void get_json_name(
    struct Country * const country,
    json_t const * const obj
){
    json_t * common = NULL;
    json_t * name = json_object_get(obj, JSON_COUNTRY_NAME);
    if(name != NULL){
        common = json_object_get(name, JSON_COMMON_NAME);
        if(common != NULL) 
            set_country_name(country, json_string_value(common));
    }
}

/**
 * Get the code of the country then affect to country
 *
 * @param country The country to which the code will be affected.
 * @param obj A json single country
 */
void get_json_code(
    struct Country * const country,
    json_t const * const obj
){
    json_t * code = json_object_get(obj, JSON_COUNTRY_CODE);
    if(code != NULL) 
        set_country_code(country, json_string_value(code));
}

/**
 * Get the capitals of the country then affect to country
 *
 * @param country The country to which the capitals will be affected.
 * @param obj A json single country
 */
void get_json_capitals(
    struct Country * const country,
    json_t const * const obj
){
    json_t * value;
    size_t index = 0;
    json_t * capitals = json_object_get(obj,JSON_COUNTRY_CAPITALS);
    if(capitals != NULL && json_typeof(capitals) == JSON_ARRAY){
        json_array_foreach(capitals, index, value) {
            add_capital(country,json_string_value(value));
        }
    }
}

/**
 * Get the borders of the country then affect to country
 *
 * @param country The country to which the borders will be affected.
 * @param obj A json single country
 */
void get_json_borders(
    struct Country * const country,
    json_t const * const obj
){
    json_t * value;
    size_t index = 0;
    json_t * borders = json_object_get(obj,JSON_COUNTRY_BORDERS);
    if(borders != NULL && json_typeof(borders) == JSON_ARRAY){
        json_array_foreach(borders, index, value) {
            add_border(country,json_string_value(value));
        }
    }
}

/**
 * Get the languages of the country then affect to country
 *
 * @param country The country to which the languages will be affected.
 * @param obj A json single country
 */
void get_json_languages(
    struct Country * const country,
    json_t const * const obj
){
    json_t * value = NULL;
    const char *key;
    json_t * langs = json_object_get(obj,JSON_COUNTRY_LANGS);
    if(langs != NULL && json_typeof(langs) == JSON_OBJECT){
        json_object_foreach(langs, key, value) {
            add_language(country,json_string_value(value));
        }
    }
}

/**
 * Get the img of the country then affect to country
 *
 * @param country The country to which the img will be affected.
 * @param obj A json single country
 */
void get_img(
    struct Country * const country
) {
    char * start_url = create_string(JSON_IMG_PATH, country->code);
    country->img_url = create_string(start_url, ".png");
    to_lower_str(country->img_url);
    free(start_url);
}

/**
 * Init a country with the specified attributes.
 *
 * @param args The Arguments struct that contain all
 *             the specifications.
 * @param country The country to load
 * @param obj A single json country
 */
void load_country(
        struct Arguments const * const args,
        struct Country * const country,
        json_t const * const obj
) {
    get_json_name(country, obj);
    get_json_code(country, obj);
    if(args->show_borders || 
       args->format == PNG ||
       args->format == DOT)
        get_json_borders(country, obj);
    if(args->show_capital)
        get_json_capitals(country, obj);
    if(args->show_languages)
        get_json_languages(country, obj);
    if((args->format == PNG || args->format == DOT)
       && args->show_flag) 
        get_img(country);
}

/**
 * Test if the current iterated json country correspond
 * to the specification.
 *
 * @param args The Arguments struct that contain all
 *             the specifications.
 * @param obj A single json country
 * @return 1 if the json country respect the specification.
 */
unsigned int condition(
    struct Arguments const * const args,
    json_t const * const obj
) {
    if(args->country_code != NULL)
        return !str_compare(args->country_code,
                json_string_value(json_object_get(obj,JSON_COUNTRY_CODE)));
    else if(args->region != NOT_SET)
        return !str_compare(regions[args->region],
                json_string_value(json_object_get(obj,JSON_REGION)));
    return 1; 
}

/**
 * Fetch all the country that correspond to the specifications.
 *
 * @param args The Arguments that contain the specifications.
 * @param root The root of the json tree.
 * @return A Countries struct.
 */
Genlist * fetch(
    struct Arguments const * const args,
    json_t const * const root
) {
    Genlist * countries = genlist_init(sizeof(struct Country*));
    struct Country * country = NULL;
    if(json_typeof(root) == JSON_ARRAY){
        size_t length = json_array_size(root);
        size_t i = 0;
        while(i < length){
            json_t * json_country_obj = json_array_get(root,i++);
            if(json_typeof(json_country_obj) == JSON_OBJECT){
                if(condition(args,json_country_obj)){
                    country = country_init();
                    load_country(args, country, json_country_obj);
                    if(!genlist_append(countries,country))
                        free_country(country);
                }
            }
        }
    }
    return countries;
}

// -------  //
// Public //
// ------- //

Genlist * load_countries(
    struct Arguments  * const args
) {
    json_t * root = NULL;
    Genlist * countries = NULL;
    json_error_t error;
    root = json_load_file(JSON_COUNTRIES_PATH, 0, &error);
    if(!root){
        args->status = TP3_JSON_ERROR;
        json_decref(root);
        return NULL;
    } 
    countries = fetch(args,root);
    json_decref(root);
    return countries;
}


