/**
 *
 * Declaration of an interface that fetch the json.
 *
 * @author Jordan Gauthier
 * @code permanent GAUJ25089201
 */

#ifndef JSON_WORKER_H
#define JSON_WORKER_H

#include "parse_args.h"
#include <jansson.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "genlist.h"
#include "country.h"

/**
 * Init a country for the specified arguments.
 *
 * @param args The Arguments struct that define
 *             the criteria for the country.
 * @return A country with the specified arguments.
 */
Genlist * load_countries(
    struct Arguments * const args
);

#endif
