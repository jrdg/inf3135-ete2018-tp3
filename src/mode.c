/**
 * Implement the mode.h interface
 *
 * @author Jordan Gauthier
 * @code permanent
 */

#include "mode.h"

// -------  //
// Private //
// ------- //

/**
 * Output the countries in text format in the specidifed stream
 *
 * @pre countries must not be null
 * @pre pfile must not be null
 * @param countries A Genlist of country
 * @param pfile The file in which you want to output the countries.
 */
void output_text(
    Genlist const * const countries,
    struct Arguments const * const args,
    FILE * pfile
) {
    size_t i = 0;
    while(i < countries->length)
        print_country((struct Country*)countries->values[i++], args, pfile);
    fprintf(pfile, "------------------------------------------\n");
}

/**
 * Output the countries in the dot format in the specified stream
 *
 * @pre countries must not be null
 * @pre pfile must not be null
 * @param countries A Genlist of country
 * @param pfile The file in which you want to output the countries
 */
void output_dot(
    Genlist * const countries,
    struct Arguments const * const args,
    FILE * pfile
) {
    Graph * graph = graph_init(countries, args);
    print_graph(graph,pfile);
    free_graph(graph);
}

/**
 * Creates the string command to use for call graphviz
 * and generating an image.
 *
 * @pre filename must not be null
 * @param filename The name of the file that will be generated
 * @return the string command
 */
char * get_graphviz_command(
    char const * const filename
) {
    char * s = GRAPHVIZ_COMMAND;
    char * s_1;
    char * s_2;
    s = create_string(s, filename);
    s_1 = create_string(s, " ");
    s_2 = create_string(s_1, PNG_TMP);
    free(s);
    free(s_1);
    return s_2;
}

/**
 * Output the countries in the PNG format in the specified stream
 *
 * @pre countries must not be null
 * @pre pfile must not be null
 * @param countries A Genlist of country
 * @param pfile The png file that will be created
 */
void output_png(
    Genlist * const countries,
    struct Arguments  * const args
) {
    FILE * tmp = fopen(PNG_TMP, "w+");
    if(tmp != NULL) {
        output_dot(countries, args, tmp);
        fclose(tmp);
        if(system(NULL)) {
            fprintf(stdout, MSG_USER_INFORMATION);
            char * scommand = get_graphviz_command(args->filename);
            system(scommand);
            free(scommand);
        }
        remove(PNG_TMP);
    }else
        args->status = TP3_FILE_ERROR;

}

/**
 * Set the file according to the value of the filename situed in args
 *
 * @pre args must not be null
 * @param pfile The adresse of a pointer that point to a file.
 * @param args The Arguments struct in which the filename is.
 */
void set_file(
    FILE ** pfile,
    struct Arguments const * const args
) {
    if(args->filename != NULL) 
        *pfile = fopen(args->filename,"w");
    else 
        *pfile = stdout;
}

/**
 * Close the specified file if it is not stdout
 *
 * @param pfile A pointer to the file
 */
void close_file(
    FILE * pfile
) {
    if(pfile != stdout)
        fclose(pfile);
}

// ------ //
// Public //
// ------ //

void execute_mode(
    struct Arguments  * const args,
    Genlist * const countries
) {
    if(args->format == PNG) {
        output_png(countries, args);
    }else {
        FILE * pfile = NULL;
        set_file(&pfile, args);
        if(pfile != NULL) {
            if(args->format == TEXT){
                output_text(countries, args, pfile);
            }else if(args->format == DOT){
                output_dot(countries, args, pfile);
            }
            close_file(pfile);
        }else
            args->status = TP3_FILE_ERROR;
    }
}
