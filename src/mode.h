/**
 * This module execute the wanted output format and 
 * call the needed functions.
 *
 * @author Jordan Gauthier
 * @code permanent GAUJ25089201
 */

#ifndef MODE_H
#define MODE_H

#include "parse_args.h"
#include "country.h"
#include "genlist.h"
#include "graph.h"
#include <unistd.h>

#define PNG_TMP "temp/tmp_png_format.dot"
#define GRAPHVIZ_COMMAND "neato -Goverlap=false -Tpng -o "
#define MSG_USER_INFORMATION "IMAGE IS BUILDING PLEASE WAIT...\n"

/**
 * Output the wanted countries in the specified format situed in args.
 *
 * @pre args must not be null
 * @pre countries must not be null
 * @param args The Arguments structure in which the specifications rules are.
 * @param countries A Genlist that contain all the countries.
 */
void execute_mode(
    struct Arguments * const args,
    Genlist * const countries
);

#endif
