/**
 * Implement the interface node.h
 *
 * @author Jordan Gauthier
 * @code permanent GAUJ25089201
 */

#include "node.h"

// -------  //
// Private //
// ------- //

#define ANGLE_OPEN '<'
#define ANGLE_CLOSE '>'
#define CURLY_OPEN '{'
#define CURLY_CLOSE '}'
#define SQUARE_OPEN '['
#define SQUARE_CLOSE ']'
#define LINK_STR "--"

/**
 * Print a Node of type LINK
 *
 * @pre node must node be null
 * @pre pfile must not be null
 * @param parent The parent of node
 * @param node The node that contain the values to print
 * @param tab The number of space to do before printing
 *            the values of the node.
 * @param pfile The file in which the node will be printed
 */
void print_link_node(
    Node const * const  parent, 
    Node const * const node,
    unsigned int tab,
    FILE * pfile
) {
    parent == NULL ||
    parent->line_type == SAME_LINE ?
    fprintf(pfile, "%s %s %s;", node->tag,
            LINK_STR, node->content) :
    fprintf(pfile, "\n%*s%s %s %s;", tab, "", 
            node->tag, LINK_STR, node->content);
}

/**
 * Print a Node of type GLOBAL
 *
 * @pre node must node be null
 * @pre pfile must not be null
 * @param parent The parent of node
 * @param node The node that contain the values to print
 * @param tab The number of space to do before printing
 *            the values of the node.
 * @param pfile The file in which the node will be printed
 */
void print_global_node(
    Node const * const parent,
    Node const * const node,
    unsigned int tab,
    FILE * pfile
) {
    parent == NULL ||
    parent->line_type == SAME_LINE ?
    fprintf(pfile, "%s ", node->tag) :
    fprintf(pfile, "\n%*s%s ", tab, "", node->tag); 
    fprintf(pfile, "%c",SQUARE_OPEN);
    for(size_t i = 0 ; i < node->childnodes->length ; i++)
        node_print_tree(node, 
            (Node*)node->childnodes->values[i],
                 tab + 4, pfile);
    node->line_type == NEW_LINE ?
    fprintf(pfile, "\n%*s%c;", tab, "", SQUARE_CLOSE) :
    fprintf(pfile, "%c;", SQUARE_CLOSE);
}

/**
 * Print a Node of type ROOT
 *
 * @pre node must not be null
 * @pre pfile must not be null
 * @param parent The parent of node
 * @param node The node that contain the values to print
 * @param tab The number of space to do before printing
 *            the values of the node.
 * @param pfile The file in which the node will be printed
 */
void print_root_node(
    Node const * const parent,
    Node const * const node,
    unsigned int tab,
    FILE * pfile
) {
    parent == NULL || 
    parent->line_type == SAME_LINE ?
    fprintf(pfile, "%s ", node->tag) :
    fprintf(pfile, "\n%*s%s ", tab, "", node->tag); 
    fprintf(pfile, "%c", CURLY_OPEN);
    for(size_t i = 0 ; i < node->childnodes->length ; i++)
        node_print_tree(node,
            (Node*)node->childnodes->values[i], 
                tab + 4, pfile);
    node->line_type == NEW_LINE ?
    fprintf(pfile, "\n%*s%c", tab, "", CURLY_CLOSE) :
    fprintf(pfile, "%c", CURLY_CLOSE);
}

/**
 * Print a Node of type OPTION
 *
 * @pre node must not be null
 * @pre pfile must not be null
 * @param node The node that contain the values to print
 * @param tab The number of space to do before printing
 *            the values of the node.
 * @param parent The parent of node
 * @paran pfile The file in which the node will be printed
 */
void print_option_node(
    Node const * const parent,
    Node const * const node,
    unsigned int tab,
    FILE * pfile
) {
    parent == NULL ||
    parent->line_type == SAME_LINE ? 
    fprintf(pfile, "%s = ", node->tag) :
    fprintf(pfile, "\n%*s%s = ", tab, "", node->tag);
    if(node->content != NULL)
        fprintf(pfile, "%s,", node->content);
    else {
        fprintf(pfile, "%c", ANGLE_OPEN);
        for(size_t i = 0 ; i < node->childnodes->length ; i++)
            node_print_tree(node, 
                (Node*)node->childnodes->values[i],
                    tab + 4, pfile);
        node->line_type == NEW_LINE ?
        fprintf(pfile, "\n%*s%c", tab , "", ANGLE_CLOSE) :
        fprintf(pfile, "%c", ANGLE_CLOSE);
    }
}

/**
 * Print the opening sequence of a Node of type HTML
 *
 * @pre node must not be null
 * @pre pfile must not be null
 * @param node The node that contain the tag
 * @param tab The number of space to do before printing the sequence
 * @param pfile The file in which the sequence will be printed
 */
void print_html_nlon_start(
    Node const * const parent, 
    Node const * const node,
    unsigned int tab,
    FILE * pfile    
) {
    parent == NULL ||
    parent->line_type == SAME_LINE ?
    fprintf(pfile, "%c%s", ANGLE_OPEN, node->tag) :
    fprintf(pfile, "\n%*s%c%s", tab , "",
                            ANGLE_OPEN, node->tag);
}

/**
 * Print the closing sequence of a Node of type HTML
 *
 * @pre node must not be null
 * @pre pfile must not be null
 * @param node The node that contain the tag
 * @param tab The number of space to do before printing the sequence
 * @param pfile The file in which the sequence will be printed
 */
void print_html_nlon_end(
    Node const * const node,
    unsigned int tab,
    FILE * pfile
) {
    if(node->line_type == NEW_LINE)
        fprintf(pfile, "\n%*s%c/%s%c", tab, "",
                 ANGLE_OPEN, node->tag, ANGLE_CLOSE);
    else if(node->braces_type == DOUBLE)
        fprintf(pfile, "%c/%s%c",
                 ANGLE_OPEN, node->tag, ANGLE_CLOSE);
    else
        fprintf(pfile, "/%c", ANGLE_CLOSE);
}

/**
 * Print the childnodes of the specified Node
 *
 * @pre node must not be null
 * @pre pfile must not be null
 * @param node The node that the childnodes are in
 * @param tab The number of space to do before printing the Node
 * @param pfile The file in which the Node will be printed
 */
void print_childnodes(
    Node const * const node,
    unsigned int tab,
    FILE * pfile
){
    size_t i = 0;
    while(i < node->childnodes->length)
        node_print_tree(node, 
        (Node*)node->childnodes->values[i++],
                             tab + 4, pfile);
}

/**
 * Print the attribute of a node then if the braces type is DOUBLE
 * print the closing bracket then print its childnodes.
 *
 * @pre node must node be null
 * @pre pfile must not be null
 * @param node The node in which the attributes and the childnodes are in
 * @param tab The number of space todo before printing the Node
 * @param pfile The file in which the Node will be printed
 */
void print_html_similiraties(
    Node const * const node,
    unsigned int tab,
    FILE * pfile
) {
    print_attrs(node->attrs, pfile);
    if(node->braces_type == DOUBLE)
        fprintf(pfile, "%c",ANGLE_CLOSE);
    print_childnodes(node, tab, pfile);
}

/**
 * Print a Node of type HTML
 *
 * @pre node must not be null
 * @pre pfile must node be null
 * @param parent The parent of that node NULL if it has no parent
 * @param node The node that will be printed
 * @param tab The number of space to do before printing the Node
 * @param pfile The file in which the Node will be printed
 */
void print_html_node(
    Node const * const parent,
    Node const * const node,
    unsigned int tab,
    FILE * pfile
) {
    print_html_nlon_start(parent, node, tab, pfile);
    print_html_similiraties(node, tab, pfile);
    print_html_nlon_end(node, tab, pfile);
}

/**
 * Print a Node of type HTML_TEXT
 *
 * @pre node must not be null
 * @pre pfile must node be null
 * @param parent The parent of that node NULL if it has no parent
 * @param node The node that will be printed
 * @param tab The number of space to do before printing the Node
 * @param pfile The file in which the Node will be printed
 */
void print_html_text_node(
    Node const * const parent,
    Node const * const node,
    unsigned int tab,
    FILE *  pfile
) {
    parent == NULL ||
    parent->line_type == SAME_LINE ?
    fprintf(pfile, "%s", node->tag) :
    fprintf(pfile, "\n%*s%s", tab, "", node->tag);
}

// ------ //
// Public //
// ------ //

Node * node_init(
    enum Node_type type,
    enum Node_braces_type braces_type,
    enum Node_line_type line_type,
    char const * const tag
) {
    Node * node = malloc(sizeof(Node));
    if(type == HTML)
        node->attrs = genlist_init(sizeof(Attr*));
    else node->attrs = NULL;
    node->childnodes = genlist_init(sizeof(Node*));
    node->content = NULL;
    node->type = type;
    node->braces_type = braces_type;
    node->line_type = line_type;
    node->tag = 
        malloc(sizeof(char) * (strlen(tag) + 1));
    str_copy(tag, node->tag);
    return node;
}

void node_set_content(
    Node * const node,
    char const * const content
) {
    node->content = 
        malloc(sizeof(char) * (strlen(content)+ 1));
    str_copy(content, node->content);
}

void node_print_tree(
    Node const * const parent,
    Node const * const root,
    unsigned int tab,
    FILE * pfile
) {
    if(root->type == GLOBAL)
        print_global_node(parent, root, tab, pfile);
    else if(root->type == OPTION)
        print_option_node(parent, root, tab, pfile);
    else if(root->type == HTML)
        print_html_node(parent, root, tab, pfile);
    else if(root->type == HTML_TEXT)
        print_html_text_node(parent, root, tab, pfile);
    else if(root->type == LINK)
        print_link_node(parent, root, tab, pfile);
    else if(root->type == ROOT)
        print_root_node(parent, root, tab, pfile);
}

unsigned int node_add_child(
    Node * const parent,
    Node * const child
) {
    return genlist_append(parent->childnodes, child);
}

unsigned int node_add_attr(
    Node * const node,
    Attr * const attr
) {
    return genlist_append(node->attrs, attr);
}

void generic_free_nodes(
    void ** values,
    size_t length
) {
    size_t i = 0;
    while(i < length)
        free_node((Node*)values[i++]);
}

void free_node(
    Node * node
) {
    free(node->tag);
    if(node->content != NULL)
        free(node->content);
    if(node->attrs != NULL)
        free_genlist(node->attrs, generic_free_attrs);
    free_genlist(node->childnodes, generic_free_nodes);
    free(node);
}
