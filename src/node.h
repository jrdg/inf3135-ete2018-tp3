/**
 * This file represent a Node interface that is useful when 
 * creating .dot file for graphviz or building html file.
 *
 * @author Jordan Gauthier
 * @code permanent GAUJ25089201
 */

#ifndef NODE_H
#define NODE_H

#include "genlist.h"
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include "attr.h"
#include "utils.h"

/**
 * This enum define the type of a Node.
 *
 * UNSPEC_TYPE : When the specification of that parameter
 *               isn't necessary. 
 * ROOT        : The root of a graph
 * GLOBAL      : A node that CAN'T contain any attributes 
 *               but can contain childnodes
 * OPTION      : An option node that can be associated with a
 *               content.
 * HTML        : An html node (can be coupled with attributes)
 * HTML_TEXT   : A  test node with no tag
 * LINK        : To make a link between to graphviz element.
 */
enum Node_type {
    UNSPEC_TYPE,
    ROOT,
    GLOBAL,
    OPTION,
    HTML,
    LINK,
    HTML_TEXT
};

/**
 * This enum define the recurence of a pair of bracket
 * for a Node.
 *
 * UNSPEC_BRACES_TYPE : When thhe specification of that parameter
 *                      isn't necessary.
 * SINGLE             : A single pair of bracket (EX : <img />)
 * DOUBLE             : A double pair of bracket (EX : <table> </table>)
 */
enum Node_braces_type {
    UNSPEC_BRACES_TYPE,
    SINGLE,
    DOUBLE
};

/**
 * define if the node will be print on the same line or will finish
 * on a new one.
 *
 * NEW_LINE  : The childnodes are print on a new line
 * SAME_LINE : The childnodes are print on the same line
 *
 */
enum Node_line_type {
    UNSPEC_LINE,
    NEW_LINE,
    SAME_LINE
};

/**
 * A Node in a graphviz tree
 *
 * @attribute type The type of the node
 * @attribute bracket_type The type of the bracket
 * @attribute braces_type The type of the braces
 * @attribute tag The tag to insert in front or between the bracket
 * @attribute content  The value of the option if the node type
 *            is OPTION or the value of the other link if the type
 *            is LINK.
 * @attribute childnodes The childs of that node
 * @attribute attrs The attributes of the node
 *
 */
typedef struct Node {
    enum Node_type type;
    enum Node_braces_type braces_type;
    enum Node_line_type line_type;
    char * tag;
    char * content;
    Genlist * childnodes;
    Genlist * attrs;
} Node;


/**
 * Init and allocate the necessary space for a Node
 *
 * @pre tag must not be null
 * @param type The type of the node
 * @param bracket_type The bracket type of the node
 * @param braces_type The braces type of the node
 * @param tag The tag of the node
 * @return A pointer to the allocated Node
 */
Node * node_init(
    enum Node_type type,
    enum Node_braces_type braces_type,
    enum Node_line_type line_type,
    char const * const tag
); 

/**
 * Set the value of the content of the specified Node.
 * The content can be associated with an Node of type
 * OPTION or LINK
 *
 * @pre content must not be null
 * @node must not be null
 * @param The Node in which the content will be set
 * @param content The string that will be affected to the content
 */
void node_set_content(
    Node * const node,
    char const * const content
);

/**
 * Print that Node and its childnodes recursively.
 *
 * @pre root must not be null
 * @pre pfile must not be null
 * @post Add a \n character at the end of the pfile stream
 * @param root A node (You should pass the ROOT node of a tree)
 * @param tab The tabulation before printing the node. (Should
 *            be 0.)
 */
void node_print_tree(
    Node const * const parent, 
    Node const * const root,
    unsigned int tab,
    FILE * pfile
);

/**
 * Add a Node to another Node
 *
 * @pre parent must not be null
 * @pre child must not be null
 * @param parent The node in which the child will be inserted
 * @param child The node that will be inserted
 * @return 1 is the child has been added 0 otherwise
 */
unsigned int node_add_child(
    Node * const parent,
    Node * const child
);

/**
 * Add an Attr to a Node
 *
 * @pre node must not be null
 * @pre attr must not be null
 * @param node The Node in which the Attr will be inserted
 * @param attr The Attr that will be inserted
 * @return 1 if the Attr has been inserted 0 otherwise.
 */
unsigned int node_add_attr(
    Node * const node,
    Attr * const attr
);

/**
 * This function need to be passed to free_genlist(). It
 * will free a Genlist of Nodes.
 *
 * @pre values must not be null
 * @param values the Nodes
 * @param length The number of element in values.
 */
void generic_free_nodes(
    void ** values,
    size_t length
);

/**
 * Free the specified node
 *
 * @pre node must not be null
 * @param node The node to free
 */
void free_node(
    Node * node
);

#endif
