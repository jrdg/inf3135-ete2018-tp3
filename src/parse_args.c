/**
 * Implement the inferface `parse_args.h`
 *
 * @author Jordan gauthier
 * @code permanent GAUJ25089201
 */

#include "parse_args.h"
#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>
#include "utils.h"
#include <string.h>

// -------  //
// Private //
// ------- //

/**
 * Init the Argument struct with his default value
 *
 * @param args The Arguments struct to initialise.
 */
void init_default_args(
    struct Arguments * args
) {
    args->status = TP3_OK;
    args->format = TEXT;
    args->filename = NULL;
    args->region = NOT_SET;
    args->country_code = NULL;
    args->show_languages = 0;
    args->show_capital = 0;
    args->show_borders = 0;
    args->show_flag = 0;
}

/**
 * Alloc the memory for the country_code attribute and set his
 * characters to those in optarg.
 * 
 * @pre country args must not be NULL
 * @pre optarg must not be NULL
 * @param args The Arguments structure in which the country_code will be set
 * @param optarg The value of the argument --country
 */
void set_country(
        struct Arguments * const args,
        char const * const optarg
){
        args->country_code = malloc(sizeof(char) * (strlen(optarg) + 1));
        str_copy_upper(optarg, args->country_code);
}

/**
 * Alloc the memory for the filename attribute and set his characters
 * to those in optarg.
 *
 * @pre country args must not be NULL
 * @pre optarg must not be NULL
 * @param args The Arguments structure in which the filename will be set
 * @param optarg The value of the argument --output-filename
 */

void set_filename(
        struct Arguments * const args,
        char const * const optarg
){
        args->filename = malloc(sizeof(char) * (strlen(optarg) + 1));
        str_copy(optarg, args->filename);
}

/**
 * Set the region code tot he specified value. If the value
 * is do not exist then the status is set to TP#_WRONG_VALUE. 
 *
 * @@pre country args must not be NULL
 * @pre optarg must not be NULL
 * @param args The Arguments structure in which the region will be set
 * @param optarg The value of the argument --region
 */
void set_region(
        struct Arguments * const args,
        char const * const optarg
){
    if(!str_compare(optarg,regions[OCEANIA]))
        args->region = OCEANIA;
    else if(!str_compare(optarg,regions[AFRICA]))
        args->region = AFRICA;
    else if(!str_compare(optarg,regions[AMERICAS]))
        args->region = AMERICAS;
    else if(!str_compare(optarg,regions[EUROPE]))
        args->region = EUROPE;
    else if(!str_compare(optarg,regions[ASIA]))
        args->region = ASIA;
    else args->status = TP3_WRONG_VALUE;
}

/**
 * Set the format to the specified value. If the value
 * do not exist then the status is set to TP3_WRONG_VALUE
 *
 * @pre country args must not be NULL
 * @pre optarg must not be NULL
 * @param args The Arguments struct in which the format will be set
 * @param optarg The value of the argument --output-format
 */
void set_format(
        struct Arguments * const args,
        char const * const optarg
){
    if(!str_compare(optarg,formats[TEXT]))
        args->format = TEXT;
    else if(!str_compare(optarg,formats[DOT]))
        args->format = DOT;
    else if(!str_compare(optarg,formats[PNG]))
        args->format = PNG;
    else args->status = TP3_WRONG_VALUE;
}


// ------ //
// Public //
// ------ //

struct Arguments *parse_arguments(
    int argc,
    char *argv[]
) {
    struct Arguments *args = 
        malloc(sizeof(struct Arguments));

    // Temporary variables
    unsigned int show_help = 0;

    // Default argument
    init_default_args(args);

    // Resets index
    optind = 0;
    struct option long_options[] = {
        {"help",            no_argument,       0, 'h'},
        {"show-languages",  no_argument,       0, 'l'},
        {"show-capital",    no_argument,       0, 'c'},
        {"show-borders",    no_argument,       0, 'b'},
        {"show-flag",       no_argument,       0, 'g'},
        {"country",         required_argument, 0, 'C'},
        {"region",          required_argument, 0, 'r'},
        {"output-format",   required_argument, 0, 'F'},
        {"output-filename", required_argument, 0, 'f'},
        {0, 0, 0, 0}
    };

    // Parse options
    while (1) {
        int option_index = 0;
        int c = getopt_long(argc, argv, "hlcbgC:r:F:f:",
                            long_options, &option_index);
        if (c == -1) break;
        switch (c) {
            case 'h':
                show_help = 1;
                break;
            case 'l':
                if(args->status == TP3_OK){
                    args->show_languages = 1;
                }
                break;
            case 'c':
                if(args->status == TP3_OK){
                    args->show_capital = 1;
                }
                break;
            case 'b':
                if(args->status == TP3_OK){
                    args->show_borders = 1;
                }
                break;
            case 'g':
                if(args->status == TP3_OK){
                    args->show_flag = 1;
                }
                break;
            case 'C':
                if(args->status == TP3_OK){
                    set_country(args,optarg);
                }
                break;
             case 'r':
                if(args->status == TP3_OK){
                    set_region(args,optarg); 
                };
                break;
            case 'F':
                if(args->status == TP3_OK){
                    set_format(args,optarg); 
                }
                break;
            case 'f':
                if(args->status == TP3_OK){
                    set_filename(args,optarg);
                }
                break;
             default:
                exit(TP3_INVALID_ARGUMENT);
        }
    }
   
    if(optind < argc){                                       // ERROR : TOO ANY ARGUMENTS
       args->status = TP3_TOO_MANY_ARGUMENTS;
        printf(TOO_MANY_ARGS_ERR);
        printf(HELP_MSG);
    }else if(show_help){
        printf(HELP_MSG);
        exit(TP3_OK);
    }else if(args->status == TP3_WRONG_VALUE){               // ERROR : WRONG VALUE
        printf(WRONG_VALUE_ERR);
        printf(HELP_MSG);
    }else if(args->format == PNG && args->filename == NULL){ // ERROR : MANDATORY FILE WITH PNG FORMAT
        printf(MANDATORY_FILE_ERR);
        printf(HELP_MSG);
        args->status = TP3_MANDATORY_FILE;
    }else if(args->country_code != NULL && args->region != NOT_SET) {
        printf(INCOMPATIBLE_ARGS_MSG);
        printf(HELP_MSG);
        args->status = TP3_INCOMPATIBLE_ARGUMENTS;
    }

    return args;
}

void print_arguments(
    struct Arguments const * const arguments
) {
    printf("struct Arguments {\n");
    printf("  Format          = %d\n", arguments->format);
    printf("  Region          = %d\n", arguments->region);
    printf("  Filename        = %s\n", arguments->filename);
    printf("  Country_code    = %s\n", arguments->country_code);
    printf("  show_languages  = %d\n", arguments->show_languages);
    printf("  show_capital    = %d\n", arguments->show_capital);
    printf("  show_border     = %d\n", arguments->show_borders);
    printf("  show_flag       = %d\n", arguments->show_flag);
    printf("  status          = %d\n", arguments->status);
    printf("}\n");
}

void print_errnrp(
    struct Arguments const * const args
) {
    if(args->status != TP3_OK) {
        if(args->status == TP3_FILE_ERROR)
            printf(FILE_ERROR_MSG);
        else if(args->status == TP3_JSON_ERROR)
            printf(JSON_ERROR_MSG);
        else if(args->status == TP3_INVALID_SPEC)
            printf(INVALID_SPEC_ERROR_MSG);
    }
}

void free_arguments(
    struct Arguments *arguments
) {
    free(arguments->filename);
    free(arguments->country_code);
    free(arguments);
}
