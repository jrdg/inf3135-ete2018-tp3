/**
* Provides basic services to process the main arguments. Error codes are also
* provided in the case of invalid input from the user.
*
* @author Jordan Gauthier (https://gitlab.com/jrdg)
* @code permanent GAUJ25089201
*/

#ifndef PARSE_ARGS_H
#define PARSE_ARGS_H

#define WRONG_VALUE_ERR "Error : wrong value.\n"
#define TOO_MANY_ARGS_ERR "Error : Too many arguments.\n"
#define MANDATORY_FILE_ERR "Error : A file is mandatory while using the png"\
                           " format.\n"
#define HELP_MSG "Usage: bin/tp3 [--help] [--output-format FORMAT] [--output-filename FILENAME]\n\
[--show-languages] [--show-capital] [--show-borders] [--show-flag]\n\
[--country COUNTRY] [--region REGION]\n\
\n\
Displays information about countries.\n\
\n\
Optional arguments:\n\
  --help                     Show this help message and exit.\n\
  --output-format FORMAT     Selects the ouput format (either \"text\", \"dot\" or \"png\").\n\
                             The \"dot\" format is the one recognized by Graphviz.\n\
                             The default format is \"text\".\n\
  --output-filename FILENAME The name of the output filename. This argument is\n\
                             mandatory for the \"png\" format. For the \"text\" and \"dot\"\n\
                             format, the result is printed on stdout if no output\n\
                             filename is given.\n\
  --show-languages           The official languages of each country are displayed.\n\
  --show-capital             The capital of each country is displayed.\n\
  --show-borders             The borders of each country are displayed.\n\
  --show-flag                The flag of each country is displayed\n\
                             (only for \"dot\" and \"png\" format).\n\
  --country COUNTRY          The country code (e.g. \"can\", \"usa\") to be displayed.\n\
  --region REGION            The region of the countries to be displayed.\n\
                             The supported regions are \"africa\", \"americas\",\n\
                             \"asia\", \"europe\" and \"oceania\".\n\
"
#define INVALID_SPEC_ERROR_MSG "Error : Zero country correspond to the"\
                               " specified specifications.\n"
#define FILE_ERROR_MSG "Error : File manipulation.\n"
#define JSON_ERROR_MSG "Error : Json manipulation.\n"
#define INCOMPATIBLE_ARGS_MSG "Error : --country and --region can't be"\
                         " specified at the same time.\n"

// Status of the program execution
enum Status {
    TP3_OK,                    // Everything is ok
    TP3_WRONG_VALUE,           // Wrong arg value 
    TP3_TOO_MANY_ARGUMENTS,    // Pass to many value to an arg
    TP3_MANDATORY_FILE,        // Mandatory file with png format
    TP3_JSON_ERROR,            // A json error
    TP3_INVALID_ARGUMENT,      // An argument is invalid
    TP3_FILE_ERROR,            // a error related to file manipulation
    TP3_INVALID_SPEC,          // When no countries have been fetched
    TP3_INCOMPATIBLE_ARGUMENTS // Some args are incompatible
};

// Type of format
enum Format {
    TEXT,
    DOT,
    PNG
};

static char const * const formats[] = {
    "text",
    "dot",
    "png"
};

// Region of the world actually supported
enum Region {
    NOT_SET,
    AFRICA,
    AMERICAS,
    ASIA,
    EUROPE,
    OCEANIA
};

static char const * const regions[] = {
    "NOT_SET",
    "Africa",
    "Americas",
    "Asia",
    "Europe",
    "Oceania"
};

/**
 * Struct that represent the arguments passed 
 * to the program,
 *
 * @attribute status The status of the program execution
 * @attribute format The format passed
 * @attribute region The region passed
 * @attribute filename The filename passed
 * @attrute country_code The cca3 of the country
 * @attribute show_language 1 if the option is passed
 * @attribute show_capital 1 if the option is passed
 * @attribute show_borders 1 if the option is passed
 * @attribute show_flag 1 if the option is passed
 */
struct Arguments {
    enum Status status;
    enum Format format;
    enum Region region;
    char * filename;
    char * country_code;
    unsigned int show_languages;
    unsigned int show_capital;
    unsigned int show_borders;
    unsigned int show_flag;
};

/**
 * Prints how to use the program.
 *
 * @param argv  The arguments provided by the user
 */
void print_usage(
    char **argv
);

/**
 * Returns the parsed arguments provided by the user.
 *
 * @param argc  The number of arguments including the program name
 * @param argv  The arguments provided by the user
 * @return      The parsed arguments
 */
struct Arguments *parse_arguments(
    int argc, 
    char **argv
);

/**
 * Prints the parsed arguments to stdout.
 *
 * @param arguments  The arguments to print
 */
void print_arguments(
    struct Arguments const * const arguments
);

/**
 * Print an error message that is not related to the 
 * parsing phase.
 *
 * @param args The arguments
 */
void print_errnrp(
    struct Arguments const * const args
);

/**
 * Frees the arguments.
 *
 * @param arguments  The arguments to free
 */
void free_arguments(
    struct Arguments *arguments
);

#endif
