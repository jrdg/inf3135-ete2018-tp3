/**
 * Testing the `attr` module with CUnit.
 *
 * @author Jordan gauthier
 * @code permanent GAUJ25089201
 */

#include "utils.h"
#include "CUnit/Basic.h"
#include "genlist.h"
#include <stdlib.h>
#include "country.h"
#include "parse_args.h"
#include "json_worker.h"
#include "mode.h"

#define TEST_ATTR "units_tests_files/test_attr.txt"

void test_attr() {
    char * filename = "temp/tmp_test_attr.txt";
    FILE * pfile = fopen(filename, "w+");
    FILE * unit_test_cpr = fopen(TEST_ATTR, "r");
    Genlist * attrs = genlist_init(sizeof(Attr*));
    Attr * bground = attr_init("background-color", "red");
    Attr * fsize = attr_init("font-size", "20");
    genlist_append(attrs, bground);
    genlist_append(attrs, fsize);
    print_attrs(attrs, pfile);
    fputc('\n', pfile);
    fseek(pfile, 0, SEEK_SET);
    CU_ASSERT_EQUAL(compare_file(pfile, unit_test_cpr), 1);
    free_genlist(attrs, generic_free_attrs);
    fclose(pfile);
    fclose(unit_test_cpr);
    CU_ASSERT_EQUAL(remove(filename), 0);
}


int main() {
    CU_pSuite pSuite = NULL;
    if (CU_initialize_registry() != CUE_SUCCESS )
        return CU_get_error();

    pSuite = CU_add_suite("Testing Attr module", NULL, NULL);
    if (pSuite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test print_attr()",
                    test_attr) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}

