/**
 * Testing the `country` module with CUnit.
 *
 * @author Jordan gauthier
 * @code permanent GAUJ25089201
 */
#include "utils.h"
#include "CUnit/Basic.h"
#include <stdlib.h>
#include "country.h"
#include <stdio.h>

void test_copy_country() {
    struct Country * cpy = country_init();
    struct Country * c1 = country_init();
    set_country_name(c1, "Canada");
    set_country_code(c1, "CAN");
    set_country_img(c1, "countries/data/can.png");
    add_capital(c1, "Ottawa");
    add_border(c1, "USA");
    add_language(c1, "French");
    add_language(c1, "English");
    copy_country(c1, cpy);
    CU_ASSERT_STRING_EQUAL(c1->name, cpy->name);
    CU_ASSERT_STRING_EQUAL(c1->code, cpy->code);
    CU_ASSERT_STRING_EQUAL(c1->img_url, cpy->img_url);
    CU_ASSERT_EQUAL(c1->capitals->length, cpy->capitals->length);
    CU_ASSERT_EQUAL(c1->borders->length, cpy->borders->length);
    CU_ASSERT_EQUAL(c1->languages->length, cpy->languages->length);
    for(size_t i = 0 ; i < c1->capitals->length ; i++)
        CU_ASSERT_STRING_EQUAL((char*)c1->capitals->values[i],
                               (char*)cpy->capitals->values[i]);
    for(size_t i = 0 ; i < c1->borders->length ; i++)
        CU_ASSERT_STRING_EQUAL((char*)c1->borders->values[i],
                               (char*)cpy->borders->values[i]);
    for(size_t i = 0 ; i < c1->capitals->length ; i++)
        CU_ASSERT_STRING_EQUAL((char*)c1->languages->values[i],
                               (char*)cpy->languages->values[i]);
    free_country(c1);
    free_country(cpy);
}

void test_set_name() {
    struct Country * country = country_init();
    set_country_name(country, "Canada");
    CU_ASSERT_STRING_EQUAL(country->name, "Canada");
    free_country(country);
}

void test_set_code() {
    struct Country * country = country_init();
    set_country_code(country, "CAN");
    CU_ASSERT_STRING_EQUAL(country->code, "CAN");
    free_country(country);
}

void test_set_img() {
    struct Country * country = country_init();
    set_country_img(country, "countries/data/can.png");
    CU_ASSERT_STRING_EQUAL(country->img_url, 
                             "countries/data/can.png");
    free_country(country);
}

void test_add_capital() {
    char * arr[] = {"test1", "test2", "test3"
                  "test4", "test5", "test6"};
    size_t len = sizeof(arr) / sizeof(arr[0]);
    struct Country * country = country_init();
    for(size_t i = 0 ; i < len ; i++)
        add_capital(country, arr[i]);
    CU_ASSERT_EQUAL(country->capitals->length, len);
    for(size_t i = 0 ; i < len ; i++)
        CU_ASSERT_STRING_EQUAL((char*)country->capitals->values[i],
                               arr[i]);
    free_country(country);
}

void test_add_border() {
    char * arr[] = {"test1", "test2", "test3"
                  "test4", "test5", "test6"};
    size_t len = sizeof(arr) / sizeof(arr[0]);
    struct Country * country = country_init();
    for(size_t i = 0 ; i < len ; i++)
        add_border(country, arr[i]);
    CU_ASSERT_EQUAL(country->borders->length, len);
    for(size_t i = 0 ; i < len ; i++)
        CU_ASSERT_STRING_EQUAL((char*)country->borders->values[i],
                               arr[i]);
    free_country(country);
}

void test_add_language() {
    char * arr[] = {"test1", "test2", "test3"
                  "test4", "test5", "test6"};
    size_t len = sizeof(arr) / sizeof(arr[0]);
    struct Country * country = country_init();
    for(size_t i = 0 ; i < len ; i++)
        add_language(country, arr[i]);
    CU_ASSERT_EQUAL(country->languages->length, len);
    for(size_t i = 0 ; i < len ; i++)
        CU_ASSERT_STRING_EQUAL((char*)country->languages->values[i],
                               arr[i]);
    free_country(country);
}

int main() {
    CU_pSuite pSuite = NULL;
    if (CU_initialize_registry() != CUE_SUCCESS )
        return CU_get_error();

    // Testing arguments parsing
    pSuite = CU_add_suite("Testing country", NULL, NULL);
    if (pSuite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test copy_country",
                    test_copy_country) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test set name",
                    test_set_name) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test set code",
                    test_set_code) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test set img url",
                    test_set_img) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test add capital",
                    test_add_capital) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test add border",
                    test_add_border) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test add language",
                    test_add_language) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}

