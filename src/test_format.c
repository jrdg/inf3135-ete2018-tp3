/**
 * Testing the `output format` with CUnit.
 *
 * @author Jordan gauthier
 * @code permanent GAUJ25089201
 */
#include "utils.h"
#include "CUnit/Basic.h"
#include "genlist.h"
#include <stdlib.h>
#include "country.h"
#include "parse_args.h"
#include "json_worker.h"
#include "mode.h"

#define TEXT_ALL_COUNTRIES "units_tests_files/all_countries.txt"
#define TEXT_SINGLE_COUNTRY "units_tests_files/single_one_country.txt"
#define TEXT_SINGLE_REGION "units_tests_files/single_region.txt"
#define DOT_ALL_COUNTRIES "units_tests_files/format_dot_test.txt"
#define DOT_SINGLE_COUNTRY "units_tests_files/format_dot_country_test.txt"
#define DOT_SINGLE_REGION "units_tests_files/format_dot_region_test.txt"

void test_format_text_all_filename() {
    char * countries_filename = "temp/tmp_all_countries.txt";
    char * argv[] = {"bin/tp3", "--show-capital",
                     "--show-borders",  "--show-languages",
                     "--output-filename" , countries_filename};
    FILE * countries_file = fopen(countries_filename,"w+");
    FILE * unit_test_cpr = fopen(TEXT_ALL_COUNTRIES,"r");
    unsigned int argc = 6;
    struct Arguments * args = parse_arguments(argc,argv);
    Genlist * countries = load_countries(args);
    execute_mode(args, countries);
    fseek(countries_file, 0, SEEK_SET);
    CU_ASSERT_EQUAL(compare_file(countries_file,unit_test_cpr), 1);
    free_genlist(countries,generic_free_countries);
    free_arguments(args);
    fclose(countries_file);
    fclose(unit_test_cpr);
    CU_ASSERT_EQUAL(remove(countries_filename), 0);
}

void test_format_text_country_filename() {
    char * countries_filename = "temp/tmp_single_one_country.txt";
    char * argv[] = {"bin/tp3", "--show-capital",
                     "--show-borders",  "--show-languages",
                     "--output-filename" , countries_filename,
                     "--country" , "can"};
    FILE * countries_file = fopen(countries_filename,"w+");
    FILE * unit_test_cpr = fopen(TEXT_SINGLE_COUNTRY,"r");
    unsigned int argc = 8;
    struct Arguments * args = parse_arguments(argc,argv);
    Genlist * countries = load_countries(args);
    execute_mode(args, countries);
    fseek(countries_file, 0, SEEK_SET);
    CU_ASSERT_EQUAL(compare_file(countries_file,unit_test_cpr), 1);
    free_genlist(countries,generic_free_countries);
    free_arguments(args);
    fclose(countries_file);
    fclose(unit_test_cpr);
    CU_ASSERT_EQUAL(remove(countries_filename), 0);
}

void test_format_text_region_filename() {
    char * countries_filename = "temp/tmp_single_region.txt";
    char * argv[] = {"bin/tp3", "--show-capital",
                     "--show-borders",  "--show-languages",
                     "--output-filename" , countries_filename,
                     "--region" , "asia"};
    FILE * countries_file = fopen(countries_filename, "w+");
    FILE * unit_test_cpr = fopen(TEXT_SINGLE_REGION, "r");
    unsigned int argc = 8;
    struct Arguments * args = parse_arguments(argc, argv);
    Genlist * countries = load_countries(args);
    execute_mode(args, countries);
    fseek(countries_file, 0, SEEK_SET);
    CU_ASSERT_EQUAL(compare_file(countries_file,unit_test_cpr), 1);
    free_genlist(countries,generic_free_countries);
    free_arguments(args);
    fclose(countries_file);
    fclose(unit_test_cpr);
    CU_ASSERT_EQUAL(remove(countries_filename), 0);
}

void test_format_dot_filename() {
    char * filename = "temp/test_format_dot.txt";
    char * argv[] = {"bin/tp3", "--output-format", "dot", "--output-filename",
                     filename, "--show-flag", "--show-borders", "--show-capital",
                     "--show-languages"};
    FILE * countries_file = fopen(filename,"w+");
    FILE * unit_test_cpr = fopen(DOT_ALL_COUNTRIES,"r");
    unsigned int argc = 9;
    struct Arguments * args = parse_arguments(argc,argv);
    Genlist * countries = load_countries(args);
    execute_mode(args, countries);
    fseek(countries_file, 0, SEEK_SET);
    CU_ASSERT_EQUAL(compare_file(countries_file, unit_test_cpr), 1);
    free_genlist(countries, generic_free_countries);
    free_arguments(args);
    fclose(countries_file);
    fclose(unit_test_cpr);
    CU_ASSERT_EQUAL(remove(filename), 0);

}

void test_format_dot_country_filename() {
    char * countries_filename = "temp/test_format_dot_country.txt";
    char * argv[] = {"bin/tp3", "--show-capital",
                     "--show-borders",  "--show-languages",
                     "--output-filename" , countries_filename,
                     "--country" , "can", "--output-format", "dot",
                     "--show-flag"};
    FILE * countries_file = fopen(countries_filename,"w+");
    FILE * unit_test_cpr = fopen(DOT_SINGLE_COUNTRY,"r");
    unsigned int argc = 11;
    struct Arguments * args = parse_arguments(argc,argv);
    Genlist * countries = load_countries(args);
    execute_mode(args, countries);
    fseek(countries_file, 0, SEEK_SET);
    CU_ASSERT_EQUAL(compare_file(countries_file,unit_test_cpr), 1);
    free_genlist(countries,generic_free_countries);
    free_arguments(args);
    fclose(countries_file);
    fclose(unit_test_cpr);
    CU_ASSERT_EQUAL(remove(countries_filename), 0);
}

void test_format_dot_region_filename() {
    char * countries_filename = "temp/test_format_dot_region.txt";
    char * argv[] = {"bin/tp3", "--show-capital",
                     "--show-borders",  "--show-languages",
                     "--output-filename" , countries_filename,
                     "--region" , "asia", "--output-format", "dot",
                     "--show-flag"};
    FILE * countries_file = fopen(countries_filename,"w+");
    FILE * unit_test_cpr = fopen(DOT_SINGLE_REGION,"r");
    unsigned int argc = 11;
    struct Arguments * args = parse_arguments(argc,argv);
    Genlist * countries = load_countries(args);
    execute_mode(args, countries);
    fseek(countries_file, 0, SEEK_SET);
    CU_ASSERT_EQUAL(compare_file(countries_file,unit_test_cpr), 1);
    free_genlist(countries,generic_free_countries);
    free_arguments(args);
    fclose(countries_file);
    fclose(unit_test_cpr);
    CU_ASSERT_EQUAL(remove(countries_filename), 0);
}

void test_format_png_filename() {
    char * filename = "temp/all_countries.png";
    char * argv[] = {"bin/tp3", "--output-format", "png",
                     "--output-filename", filename, "--show-borders",
                     "--show-flag", "--show-languages", "--show-capital"};
    FILE * file = NULL;
    unsigned int argc = 9;
    struct Arguments * args = parse_arguments(argc, argv);
    Genlist * countries = load_countries(args);
    execute_mode(args, countries);
    free_genlist(countries, generic_free_countries);
    free_arguments(args);
    CU_ASSERT_NOT_EQUAL((file = fopen(filename, "r")), NULL);
    if(file != NULL)
        fclose(file);
    CU_ASSERT_EQUAL(remove(filename), 0);
}

void test_format_png_country_filename() {
    char * filename = "temp/can.png";
    char * argv[] = {"bin/tp3", "--output-format", "png",
                     "--output-filename", filename, "--show-borders",
                     "--show-flag", "--show-languages", "--show-capital",
                     "--country", "can"};
    FILE * file = NULL;
    unsigned int argc = 11;
    struct Arguments * args = parse_arguments(argc, argv);
    Genlist * countries = load_countries(args);
    execute_mode(args, countries);
    free_genlist(countries, generic_free_countries);
    free_arguments(args);
    CU_ASSERT_NOT_EQUAL((file = fopen(filename, "r")), NULL);
    if(file != NULL)
        fclose(file);
    CU_ASSERT_EQUAL(remove(filename), 0);
}

void test_format_png_region_filename() {
    char * filename = "temp/region.png";
    char * argv[] = {"bin/tp3", "--output-format", "png",
                     "--output-filename", filename, "--show-borders",
                     "--show-flag", "--show-languages", "--show-capital",
                      "--region", "americas"};
    FILE * file = NULL;
    unsigned int argc = 11;
    struct Arguments * args = parse_arguments(argc, argv);
    Genlist * countries = load_countries(args);
    execute_mode(args, countries);
    free_genlist(countries, generic_free_countries);
    free_arguments(args);
    CU_ASSERT_NOT_EQUAL((file = fopen(filename, "r")), NULL);
    if(file != NULL)
        fclose(file);
    CU_ASSERT_EQUAL(remove(filename), 0);
}

int main() {
    CU_pSuite pSuite = NULL;
    if (CU_initialize_registry() != CUE_SUCCESS )
        return CU_get_error();

    // Testing arguments parsing
    pSuite = CU_add_suite("Testing format", NULL, NULL);
    if (pSuite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test format text with all countries and all options and filename",
                    test_format_text_all_filename) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test format text with a single country and all options and filename",
                    test_format_text_country_filename) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test format text with a single region and all options and filename",
                    test_format_text_region_filename) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test format dot with all countries all options and filename",
                    test_format_dot_filename) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test format dot with a single country and all  options and filename",
                    test_format_dot_country_filename) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test format dot with a single region and all options and filename",
                    test_format_dot_region_filename) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test format png with all countries and all options",
                    test_format_png_filename) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test format png with a single country and all options",
                    test_format_png_country_filename) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test format png with a single region and all options",
                    test_format_png_region_filename) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}

