/**
 * Testing the `genlist` module with CUnit.
 *
 * @author Jordan gauthier
 * @code permanent GAUJ25089201
 */
#include "utils.h"
#include "CUnit/Basic.h"
#include "genlist.h"
#include <stdlib.h>

void free_names(void** values, size_t length){
    size_t i = 0;
    while(i < length)
        free(values[i++]);
}

void test_genlist() {
    Genlist * names = genlist_init(sizeof(char*));
    size_t length = 11;
    size_t i = 0;
    char * ptr = NULL;
    char * pre[11] = { 
        "jordan", "charles", "audrey",
        "sebastien", "alexandre", "jean",
        "hugo", "erdos", "samuel",
        "danielle", "hubert"
    };
    while(i < length){
        ptr = malloc(sizeof(char*) * (strlen(pre[i]) + 1));
        str_copy(pre[i],ptr);
        genlist_append(names,ptr);
        i++;
    }
    CU_ASSERT_EQUAL(names->length,   length);
    CU_ASSERT_EQUAL(names->index,    length);
    CU_ASSERT_EQUAL(names->capacity, 20);
    i = 0;
    while(i < names->length) {
        CU_ASSERT_STRING_EQUAL(names->values[i], pre[i]);
        i++;
    }
    free_genlist(names,free_names);
}

int main() {
    CU_pSuite pSuite = NULL;
    if (CU_initialize_registry() != CUE_SUCCESS )
        return CU_get_error();

    // Testing arguments parsing
    pSuite = CU_add_suite("Testing Genlist", NULL, NULL);
    if (pSuite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Global test",
                    test_genlist) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}

