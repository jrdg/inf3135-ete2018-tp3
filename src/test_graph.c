/**
 * Testing the `graph` module with CUnit.
 *
 * @author Jordan gauthier
 * @code permanent GAUJ25089201
 */
#include "utils.h"
#include "CUnit/Basic.h"
#include "genlist.h"
#include <stdlib.h>
#include "country.h"
#include "parse_args.h"
#include "json_worker.h"
#include "mode.h"

#define TEST_GRAPH "units_tests_files/test_graph.txt"

void test_graph() {
    char * filename = "temp/tmp_test_graph.txt";
    FILE * pfile = fopen(filename, "w+");
    FILE * unit_test_cpr = fopen(TEST_GRAPH, "r");
    Genlist * countries = genlist_init(sizeof(struct Country*));
    struct Arguments * args = malloc(sizeof(struct Arguments));
    args->show_borders = 1;

    struct Country * can = country_init();
    set_country_name(can, "Canada");
    set_country_code(can, "CAN");
    set_country_img(can, "countries/data/can.png");
    add_capital(can, "Ottawa");
    add_language(can, "French");
    add_language(can, "English");
    add_border(can, "USA");

    struct Country * usa = country_init();
    set_country_name(usa, "United States");
    set_country_code(usa, "USA");
    set_country_img(usa, "countries/data/usa.png");
    add_capital(usa, "Washington D.C.");
    add_language(usa, "English");
    add_border(usa, "CAN");
    add_border(usa, "MEX");

    struct Country * mex = country_init();
    set_country_name(mex, "Mexico");
    set_country_code(mex, "MEX");
    set_country_img(mex, "countries/data/mex.png");
    add_capital(mex, "Mexico City");
    add_language(mex, "Spanish");
    add_border(mex, "BLZ");
    add_border(mex, "GTM");
    add_border(mex, "USA");

    struct Country * blz = country_init();
    set_country_name(blz, "Belize");
    set_country_code(blz, "BLZ");
    set_country_img(blz, "countries/data/blz.png");
    add_capital(blz, "Belmopan");
    add_language(blz, "Belizean Creole");
    add_language(blz, "English");
    add_language(blz, "Spanish");
    add_border(blz, "GTM");
    add_border(blz, "MEX");

    struct Country * gtm = country_init();
    set_country_name(gtm, "Guatemala");
    set_country_code(gtm, "GTM");
    set_country_img(gtm, "countries/data/gtm.png");
    add_capital(gtm, "Guatemala City");
    add_language(gtm, "Spanish");
    add_border(gtm, "BLZ");
    add_border(gtm, "SLV");
    add_border(gtm, "HND");
    add_border(gtm, "MEX");

    genlist_append(countries, usa);
    genlist_append(countries, mex);
    genlist_append(countries, gtm);
    genlist_append(countries, can);
    genlist_append(countries, blz);

    Graph * graph = graph_init(countries, args);
    print_graph(graph, pfile);
    fseek(pfile, 0, SEEK_SET);
    CU_ASSERT_EQUAL(compare_file(pfile, unit_test_cpr), 1);
    free_graph(graph);
    free_genlist(countries, generic_free_countries);
    fclose(pfile);
    fclose(unit_test_cpr);
    CU_ASSERT_EQUAL(remove(filename), 0);
}


int main() {
    CU_pSuite pSuite = NULL;
    if (CU_initialize_registry() != CUE_SUCCESS )
        return CU_get_error();

    // Testing arguments parsing
    pSuite = CU_add_suite("Testing GRAPH", NULL, NULL);
    if (pSuite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test GRAPH",
                    test_graph) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}

