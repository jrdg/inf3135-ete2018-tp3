/**
 * Testing the `node` module with CUnit.
 *
 * @author Jordan gauthier
 * @code permanent GAUJ25089201
 */
#include "utils.h"
#include "CUnit/Basic.h"
#include <stdlib.h>
#include "node.h"
#include <stdio.h>

#define ROOT_TEST_FILE "units_tests_files/root_test_node.txt"
#define ROOT_TEST_SL_FILE "units_tests_files/root_sl_test_node.txt"
#define HTML_TEST_FILE "units_tests_files/html_test_node.txt"
#define HTML_SINGLE_TEST_FILE "units_tests_files/html_single_test_node.txt"
#define GLOBAL_TEST_FILE "units_tests_files/global_test_node.txt"
#define GLOBAL_TEST_SL_FILE "units_tests_files/global_sl_test_node.txt"
#define HTML_TREE_TEST "units_tests_files/html_tree_node_test.txt"

/**
 * {, [ , <
 * root , global , html, option
 * double, single
 */
void test_node_root() {
    char * filename = "temp/tmp_root_test.txt";
    Node * root = node_init(ROOT, UNSPEC_BRACES_TYPE,  NEW_LINE, "graph");
    FILE * tmp_file = fopen(filename,"w+");
    FILE * unit_test_cpr = fopen(ROOT_TEST_FILE,"r");
    node_print_tree(NULL, root,0,tmp_file);
    fputc('\n',tmp_file);
    fseek(tmp_file, 0, SEEK_SET);
    CU_ASSERT_EQUAL(compare_file(tmp_file, unit_test_cpr), 1);
    fclose(tmp_file);
    fclose(unit_test_cpr);
    CU_ASSERT_EQUAL(remove(filename), 0);
}

void test_node_global() {
    char * filename = "temp/tmp_global_test.txt";
    Node * root = node_init(GLOBAL, UNSPEC_BRACES_TYPE, NEW_LINE, "can");
    FILE * tmp_file = fopen(filename,"w+");
    FILE * unit_test_cpr = fopen(GLOBAL_TEST_FILE,"r");
    node_print_tree(NULL, root,0,tmp_file);
    fputc('\n',tmp_file);
    fseek(tmp_file, 0, SEEK_SET);
    CU_ASSERT_EQUAL(compare_file(tmp_file, unit_test_cpr), 1);
    fclose(tmp_file);
    fclose(unit_test_cpr);
    CU_ASSERT_EQUAL(remove(filename), 0);
}

void test_node_root_sl() {
    char * filename = "temp/tmp_root_test.txt";
    Node * root = node_init(ROOT, UNSPEC_BRACES_TYPE,  SAME_LINE, "graph");
    FILE * tmp_file = fopen(filename,"w+");
    FILE * unit_test_cpr = fopen(ROOT_TEST_SL_FILE,"r");
    node_print_tree(NULL, root,0,tmp_file);
    fputc('\n',tmp_file);
    fseek(tmp_file, 0, SEEK_SET);
    CU_ASSERT_EQUAL(compare_file(tmp_file, unit_test_cpr), 1);
    fclose(tmp_file);
    fclose(unit_test_cpr);
    CU_ASSERT_EQUAL(remove(filename), 0);
}

void test_node_global_sl() {
    char * filename = "temp/tmp_global_test.txt";
    Node * root = node_init(GLOBAL, UNSPEC_BRACES_TYPE, SAME_LINE, "can");
    FILE * tmp_file = fopen(filename,"w+");
    FILE * unit_test_cpr = fopen(GLOBAL_TEST_SL_FILE,"r");
    node_print_tree(NULL, root,0,tmp_file);
    fputc('\n',tmp_file);
    fseek(tmp_file, 0, SEEK_SET);
    CU_ASSERT_EQUAL(compare_file(tmp_file, unit_test_cpr), 1);
    fclose(tmp_file);
    fclose(unit_test_cpr);
    CU_ASSERT_EQUAL(remove(filename), 0);
}

void test_node_html() {
    char * filename = "temp/tmp_html_test.txt";
    Node * root = node_init(HTML, DOUBLE, NEW_LINE, "div");
    FILE * tmp_file = fopen(filename,"w+");
    FILE * unit_test_cpr = fopen(HTML_TEST_FILE,"r");
    node_print_tree(NULL, root,0,tmp_file);
    fputc('\n',tmp_file);
    fseek(tmp_file, 0, SEEK_SET);
    CU_ASSERT_EQUAL(compare_file(tmp_file, unit_test_cpr), 1);
    fclose(tmp_file);
    fclose(unit_test_cpr);
    CU_ASSERT_EQUAL(remove(filename), 0);
}

void test_node_html_same_line() {
    char * filename = "temp/tmp_html_sl_test.txt";
    Node * root = node_init(HTML, SINGLE, SAME_LINE, "img");
    FILE * tmp_file = fopen(filename,"w+");
    FILE * unit_test_cpr = fopen(HTML_SINGLE_TEST_FILE,"r");
    node_print_tree(NULL, root,0,tmp_file);
    fputc('\n',tmp_file);
    fseek(tmp_file, 0, SEEK_SET);
    CU_ASSERT_EQUAL(compare_file(tmp_file, unit_test_cpr), 1);
    fclose(tmp_file);
    fclose(unit_test_cpr);
    CU_ASSERT_EQUAL(remove(filename), 0);
}

void test_html_tree() {
    char * filename = "temp/tmp_html_sl_test.txt";
    FILE * tmp_file = fopen(filename,"w+");
    FILE * unit_test_cpr = fopen(HTML_TREE_TEST, "r");
    Node * root = node_init(HTML, DOUBLE, NEW_LINE, "table");
    Node * tr = NULL;
    Node * td = NULL;
    Node * text = NULL;
    size_t i = 0;
    while(i < 5) {
        tr = node_init(HTML, DOUBLE, NEW_LINE, "tr");
        td = node_init(HTML, DOUBLE, NEW_LINE, "td");
        text = node_init(HTML_TEXT, UNSPEC_BRACES_TYPE,
                                      UNSPEC_LINE, 
                                        "Some text");
        genlist_append(td->childnodes, text);
        genlist_append(tr->childnodes, td);
        genlist_append(root->childnodes, tr);
        i++;
    }
    node_print_tree(NULL, root, 0 , tmp_file);
    fputc('\n',tmp_file);
    fseek(tmp_file, 0, SEEK_SET);
    CU_ASSERT_EQUAL(compare_file(tmp_file, unit_test_cpr), 1);
    fclose(tmp_file);
    fclose(unit_test_cpr);
    CU_ASSERT_EQUAL(remove(filename), 0);
}

int main() {
    CU_pSuite pSuite = NULL;
    if (CU_initialize_registry() != CUE_SUCCESS )
        return CU_get_error();

    // Testing arguments parsing
    pSuite = CU_add_suite("Testing node", NULL, NULL);
    if (pSuite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test Node type ROOT",
                    test_node_root) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test Node type GLOBAL",
                    test_node_global) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test Node type ROOT SAME_LINE",
                    test_node_root_sl) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test Node type GLOBAL SAME_LINE",
                    test_node_global_sl) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
     if (CU_add_test(pSuite, "Test Node type HTML and braces DOUBLE",
                    test_node_html) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test Node type HTML and braces SINGLE",
                    test_node_html_same_line) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Test Node type HTML tree",
                    test_html_tree) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}

