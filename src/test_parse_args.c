/**
 * Testing the `parse_args` module with CUnit.
 *
 * @author Jordan gauthier
 * @code permanent GAUJ25089201
 */
#include "parse_args.h"
#include "CUnit/Basic.h"


void test_show_default() {
    char *argv[] = {"bin/tp3", "--show-languages", "--show-flag" ,
                    "--show-borders" , "--show-capital"};
    int argc = 5;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->show_languages, 1);
    CU_ASSERT_EQUAL(arguments->show_flag,      1);
    CU_ASSERT_EQUAL(arguments->show_borders,   1);
    CU_ASSERT_EQUAL(arguments->show_capital,   1);
    CU_ASSERT_EQUAL(arguments->country_code,   NULL);
    CU_ASSERT_EQUAL(arguments->region,         NOT_SET);
    CU_ASSERT_EQUAL(arguments->format,         TEXT);
    CU_ASSERT_EQUAL(arguments->filename,       NULL);
    CU_ASSERT_EQUAL(arguments->status,         TP3_OK);
    free_arguments(arguments);
}

void test_show_languages() {
    char *argv[] = {"bin/tp3", "--show-languages"};
    int argc = 2;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->show_languages, 1);
    CU_ASSERT_EQUAL(arguments->show_flag,      0);
    CU_ASSERT_EQUAL(arguments->show_borders,   0);
    CU_ASSERT_EQUAL(arguments->show_capital,   0);
    CU_ASSERT_EQUAL(arguments->country_code,   NULL);
    CU_ASSERT_EQUAL(arguments->region,         NOT_SET);
    CU_ASSERT_EQUAL(arguments->format,         TEXT);
    CU_ASSERT_EQUAL(arguments->filename,       NULL);
    CU_ASSERT_EQUAL(arguments->status,         TP3_OK);
    free_arguments(arguments);
}
void test_show_borders() {
    char *argv[] = {"bin/tp3", "--show-borders" };
    int argc = 2;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->show_languages, 0);
    CU_ASSERT_EQUAL(arguments->show_flag,      0);
    CU_ASSERT_EQUAL(arguments->show_borders,   1);
    CU_ASSERT_EQUAL(arguments->show_capital,   0);
    CU_ASSERT_EQUAL(arguments->country_code,   NULL);
    CU_ASSERT_EQUAL(arguments->region,         NOT_SET);
    CU_ASSERT_EQUAL(arguments->format,         TEXT);
    CU_ASSERT_EQUAL(arguments->filename,       NULL);
    CU_ASSERT_EQUAL(arguments->status,         TP3_OK);
    free_arguments(arguments);
}
void test_show_capital() {
    char *argv[] = {"bin/tp3", "--show-capital"};
    int argc = 2;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->show_languages, 0);
    CU_ASSERT_EQUAL(arguments->show_flag,      0);
    CU_ASSERT_EQUAL(arguments->show_borders,   0);
    CU_ASSERT_EQUAL(arguments->show_capital,   1);
    CU_ASSERT_EQUAL(arguments->country_code,   NULL);
    CU_ASSERT_EQUAL(arguments->region,         NOT_SET);
    CU_ASSERT_EQUAL(arguments->format,         TEXT);
    CU_ASSERT_EQUAL(arguments->filename,       NULL);
    CU_ASSERT_EQUAL(arguments->status,         TP3_OK);
    free_arguments(arguments);
}

void test_show_flag() {
    char *argv[] = {"bin/tp3", "--show-flag"};
    int argc = 2;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->show_languages, 0);
    CU_ASSERT_EQUAL(arguments->show_flag,      1);
    CU_ASSERT_EQUAL(arguments->show_borders,   0);
    CU_ASSERT_EQUAL(arguments->show_capital,   0);
    CU_ASSERT_EQUAL(arguments->country_code,   NULL);
    CU_ASSERT_EQUAL(arguments->region,         NOT_SET);
    CU_ASSERT_EQUAL(arguments->format,         TEXT);
    CU_ASSERT_EQUAL(arguments->filename,       NULL);
    CU_ASSERT_EQUAL(arguments->status,         TP3_OK);
    free_arguments(arguments);
}

void test_country() {
    char *argv[] = {"bin/tp3", "--country" , "can"};
    int argc = 3;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->show_languages, 0);
    CU_ASSERT_EQUAL(arguments->show_flag,      0);
    CU_ASSERT_EQUAL(arguments->show_borders,   0);
    CU_ASSERT_EQUAL(arguments->show_capital,   0);
    CU_ASSERT_STRING_EQUAL(arguments->country_code, "CAN");
    CU_ASSERT_EQUAL(arguments->region,         NOT_SET);
    CU_ASSERT_EQUAL(arguments->format,         TEXT);
    CU_ASSERT_EQUAL(arguments->filename,       NULL);
    CU_ASSERT_EQUAL(arguments->status,         TP3_OK);
    free_arguments(arguments);

}

void test_filename(){
    char *argv[] = {"bin/tp3", "--output-filename" , "fichier.txt"};
    int argc = 3;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->show_languages, 0);
    CU_ASSERT_EQUAL(arguments->show_flag,      0);
    CU_ASSERT_EQUAL(arguments->show_borders,   0);
    CU_ASSERT_EQUAL(arguments->show_capital,   0);
    CU_ASSERT_EQUAL(arguments->country_code,   NULL);
    CU_ASSERT_EQUAL(arguments->region,         NOT_SET);
    CU_ASSERT_EQUAL(arguments->format,         TEXT);
    CU_ASSERT_STRING_EQUAL(arguments->filename,"fichier.txt");
    CU_ASSERT_EQUAL(arguments->status,         TP3_OK);
    free_arguments(arguments);
}

void test_region() {
    char *argv[] = {"bin/tp3", "--region" , "americas"};
    int argc = 3;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->show_languages, 0);
    CU_ASSERT_EQUAL(arguments->show_flag,      0);
    CU_ASSERT_EQUAL(arguments->show_borders,   0);
    CU_ASSERT_EQUAL(arguments->show_capital,   0);
    CU_ASSERT_EQUAL(arguments->country_code,   NULL);
    CU_ASSERT_EQUAL(arguments->region,         AMERICAS);
    CU_ASSERT_EQUAL(arguments->format,         TEXT);
    CU_ASSERT_EQUAL(arguments->filename,       NULL);
    CU_ASSERT_EQUAL(arguments->status,         TP3_OK);
    free_arguments(arguments);
}

void test_format(){
    char *argv[] = {"bin/tp3", "--output-format" , "png" , 
                    "--output-filename" , "fichier.txt"};
    int argc = 5;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->show_languages, 0);
    CU_ASSERT_EQUAL(arguments->show_flag,      0);
    CU_ASSERT_EQUAL(arguments->show_borders,   0);
    CU_ASSERT_EQUAL(arguments->show_capital,   0);
    CU_ASSERT_EQUAL(arguments->country_code,   NULL);
    CU_ASSERT_EQUAL(arguments->region,         NOT_SET);
    CU_ASSERT_EQUAL(arguments->format,         PNG);
    CU_ASSERT_STRING_EQUAL(arguments->filename, "fichier.txt");
    CU_ASSERT_EQUAL(arguments->status,         TP3_OK);
    free_arguments(arguments);
}

void test_region_err() {
    char *argv[] = {"bin/tp3", "--region" , "south americas"};
    int argc = 3;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->show_languages, 0);
    CU_ASSERT_EQUAL(arguments->show_flag,      0);
    CU_ASSERT_EQUAL(arguments->show_borders,   0);
    CU_ASSERT_EQUAL(arguments->show_capital,   0);
    CU_ASSERT_EQUAL(arguments->country_code,   NULL);
    CU_ASSERT_EQUAL(arguments->region,         NOT_SET);
    CU_ASSERT_EQUAL(arguments->format,         TEXT);
    CU_ASSERT_EQUAL(arguments->filename,       NULL);
    CU_ASSERT_EQUAL(arguments->status,         TP3_WRONG_VALUE);
    free_arguments(arguments);
}

void test_format_err(){
    char *argv[] = {"bin/tp3", "--output-format" , "numeric"};
    int argc = 3;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->show_languages, 0);
    CU_ASSERT_EQUAL(arguments->show_flag,      0);
    CU_ASSERT_EQUAL(arguments->show_borders,   0);
    CU_ASSERT_EQUAL(arguments->show_capital,   0);
    CU_ASSERT_EQUAL(arguments->country_code,   NULL);
    CU_ASSERT_EQUAL(arguments->region,         NOT_SET);
    CU_ASSERT_EQUAL(arguments->format,         TEXT);
    CU_ASSERT_EQUAL(arguments->filename,       NULL);
    CU_ASSERT_EQUAL(arguments->status,         TP3_WRONG_VALUE);
    free_arguments(arguments);
}

void test_all_options(){
    char *argv[] = {"bin/tp3", "--output-format" , "png",
                    "--output-filename" , "canada.png",
                    "--region" , "americas" , "--country",
                    "can" , "--show-languages", "--show-flag",
                    "--show-borders", "--show-capital"};
    int argc = 13;
    struct Arguments *arguments = parse_arguments(argc, argv);
    CU_ASSERT_EQUAL(arguments->show_languages,      1);
    CU_ASSERT_EQUAL(arguments->show_flag,           1);
    CU_ASSERT_EQUAL(arguments->show_borders,        1);
    CU_ASSERT_EQUAL(arguments->show_capital,        1);
    CU_ASSERT_STRING_EQUAL(arguments->country_code, "CAN")
    CU_ASSERT_EQUAL(arguments->region,              AMERICAS);
    CU_ASSERT_EQUAL(arguments->format,              PNG);
    CU_ASSERT_STRING_EQUAL(arguments->filename,     "canada.png")
    CU_ASSERT_EQUAL(arguments->status,              TP3_INCOMPATIBLE_ARGUMENTS);
    free_arguments(arguments);
}


int main() {
    CU_pSuite pSuite = NULL;
    if (CU_initialize_registry() != CUE_SUCCESS )
        return CU_get_error();

    // Testing arguments parsing
    pSuite = CU_add_suite("Testing arguments parsing", NULL, NULL);
    if (pSuite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking no error all shows options with default value",
                    test_show_default) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking no error --show-languages with default value",
                    test_show_languages) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking no error --show-flag with default value",
                    test_show_flag) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking no error --show-capital with default value",
                    test_show_capital) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking no error --show-borders with default value",
                    test_show_borders) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking no error --country with default value",
                    test_country) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking no error --output-filename",
                    test_filename) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking no error --region",
                    test_region) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking no error --output-format",
                    test_format) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking error --output-format",
                    test_format_err) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking error --region",
                    test_region_err) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Checking no error all options",
                    test_all_options) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }




    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}

