/**
 * Testing the `utils` module with CUnit.
 *
 * @author Jordan gauthier
 * @code permanent GAUJ25089201
 */
#include "utils.h"
#include "CUnit/Basic.h"
#include "genlist.h"

void test_app_genlist_tstr() {
    char * initial_string = "Borders : ";
    char * newstring = NULL;
    Genlist * list = genlist_init(sizeof(char*));
    genlist_append(list, "can");
    genlist_append(list, "usa");
    genlist_append(list, "arg");
    genlist_append(list, "afg");
    genlist_append(list, "gbr");
    genlist_append(list, "fra");
    newstring = app_genlist_tstr(initial_string, list, 0);
    CU_ASSERT_STRING_EQUAL(newstring, 
            "Borders : can, usa, arg, afg, gbr, fra");
    free(newstring);
    free_genlist(list, NULL);
}

void test_str_copy() {
    char * source = "tp3";
    char target[4];
    str_copy(source,target);
    CU_ASSERT_STRING_EQUAL(target, source);
}

void test_str_compare_neg() {
    char * s1 = "a";
    char * s2 = "b";
    CU_ASSERT(str_compare(s1,s2) < 0);
    s1 = "allo";
    s2 = "alloo";
    CU_ASSERT(str_compare(s1,s2) < 0);
    s1 = "ballon";
    s2 = "ballzo";
    CU_ASSERT(str_compare(s1,s2) < 0);
}

void test_str_compare_zero() {
    char * s1 = "a";
    char * s2 = "a";
    CU_ASSERT(str_compare(s1,s2) == 0);
    s1 = "allo";
    s2 = "allo";
    CU_ASSERT(str_compare(s1,s2) == 0);
    s1 = "ballon";
    s2 = "ballon";
    CU_ASSERT(str_compare(s1,s2) == 0);

}

void test_str_compare_pos() {
    char * s1 = "b";
    char * s2 = "a";
    CU_ASSERT(str_compare(s1,s2) > 0);
    s1 = "alloo";
    s2 = "allo";
    CU_ASSERT(str_compare(s1,s2) > 0);
    s1 = "ballzn";
    s2 = "ballon";
    CU_ASSERT(str_compare(s1,s2) > 0);

}

int main() {
    CU_pSuite pSuite = NULL;
    if (CU_initialize_registry() != CUE_SUCCESS )
        return CU_get_error();

    // Testing arguments parsing
    pSuite = CU_add_suite("Testing utils", NULL, NULL);
    if (pSuite == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "Copying string test",
                    test_str_copy) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "test when s1 is smaller",
                    test_str_compare_neg) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "test when s1 == s2",
                    test_str_compare_zero) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "test when s2 is smaller",
                    test_str_compare_pos) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    if (CU_add_test(pSuite, "app_genlist_tstr",
                    test_app_genlist_tstr) == NULL) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}

