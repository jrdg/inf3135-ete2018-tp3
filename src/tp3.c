/**
 * tp3.c
 *
 * This program generate a file of informations about a group 
 * of countries according to a given output and specifications.
 * It can generate a PNG file, .dot file or simply printing the
 * wanted result on stdout.
 *
 * Exemple TEXT format :
 *
 * command entered :
 *
 * `bin/tp3 --country can --show-borders --show-language --show-capital`
 *
 * Then the output :
 *
 * ```
 * ------------------------------------------
 *   Name      : Canada
 *   Code      : CAN
 *   Capitals  :
 *       - Ottawa
 *   Languages :
 *       - English
 *       - French
 *   Borders   :
 *       - USA
 *  ------------------------------------------
 * ```
 *
 * Exemple DOT format :
 *
 * `bin/tp3 --show-capital --show-borders --show-languages --show-flag
 *  --country can --output-format dot`
 *
 *  Then the output :
 *
 * ```
 * graph {
 *     CAN [
 *         shape = none,
 *         label = <
 *             <table border="0" cellspacing="0">
 *                 <tr>
 *                     <td align="center" border="1"><img src="countries/data/can.png" scale="true"/></td>
 *                 </tr>
 *                 <tr>
 *                     <td align="left" border="1"><b>Name : </b>Canada</td>
 *                 </tr>
 *                 <tr>
 *                     <td align="left" border="1"><b>Code : </b>CAN</td>
 *                 </tr>
 *                 <tr>
 *                     <td align="left" border="1"><b>Capitals : </b>Ottawa</td>
 *                 </tr>
 *                 <tr>
 *                     <td align="left" border="1"><b>Languages : </b>English, French</td>
 *                 </tr>
 *                 <tr>
 *                     <td align="left" border="1"><b>Borders : </b>USA</td>
 *                 </tr>
 *             </table>
 *         >
 *     ];
 * }
 * ```
 *
 * Exemple PNG format :
 *
 * The PNG format has the same behavior that the dot format with
 * the exeption for gneerating the file automaticaly without the
 * need of passing the generated output to graphviz.
 *
 *
 * To know more about this software visit :
 *
 *     - https://gitlab.com/jrdg/inf3135-ete2018-tp3
 *
 * @author Jordan Gauthier
 * @code permanent GAUJ25089201
 * @version 1.0
 */

#include <stdio.h>
#include "parse_args.h"
#include <jansson.h>
#include <string.h>
#include "utils.h"
#include "genlist.h"
#include "json_worker.h"
#include "mode.h"

int main(int argc, char * argv[]){
    int status = TP3_OK;
    struct Arguments * args = parse_arguments(argc,argv);
    if(args->status == TP3_OK){
        Genlist * countries = load_countries(args);
        if(countries != NULL) {
            if(countries->length > 0)
                execute_mode(args, countries);
            else
                args->status = TP3_INVALID_SPEC;
            free_genlist(countries,generic_free_countries);
        }
    }
    status = args->status;
    print_errnrp(args);
    free_arguments(args);
    return status;
}
