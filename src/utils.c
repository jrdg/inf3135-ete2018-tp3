/**
 * Implement the interface `utils.h`
 *
 * @author Jordan Gauthier
 * @code permanent GAUJ25089201
 */

#include "utils.h"

char * app_genlist_tstr(
    char const * const string,
    Genlist const * const list,
    size_t index
) {
    char * newstring = NULL;
    char * newstring2 = NULL;
    char * tr = NULL;
    if(index < list->length){
        newstring = create_string(string,
                (char*)list->values[index]);
        if(index == list->length-1){
            tr = create_string(newstring, "");
        }else{
            newstring2 = create_string(newstring, ", ");
            tr = app_genlist_tstr(newstring2, 
                                    list, index + 1);
            free(newstring2);
        }
        free(newstring);
    }
    return tr;
}

void to_lower_str(
    char * str
) {
    size_t len = strlen(str);
    size_t i = 0;
    while(i < len) {
        str[i] = tolower(str[i]);
        i++;
    }
}

char * create_string(
    char const * const s1,
    char const * const s2
) {
    size_t len_s1 = strlen(s1);
    size_t len_s2 = strlen(s2);
    size_t total = len_s1 + len_s2;
    char * newstring = 
        malloc(sizeof(char) * (total + 1));
    for(size_t i = 0 ; i < total ; i++){
        if(i < len_s1) newstring[i] = s1[i];
        else newstring[i] = s2[i - len_s1];
    }
    newstring[total] = '\0';
    return newstring;
}

void str_copy(
    char const * const  source,
    char * const target
) {
    size_t i = 0;
    do{
        target[i] = source[i];
    } while(source[i++] != '\0');
}

void str_copy_upper(
    char const * const  source,
    char * const target
) {
    size_t i = 0;
    do{
        target[i] = toupper(source[i]);
    } while(source[i++] != '\0');
}

signed int str_compare(
    char const * const s_1,
    char const * const s_2
) {
    size_t s1_len = strlen(s_1);
    size_t s2_len = strlen(s_2);
    char * s1 =
        malloc(sizeof(char) * (s1_len + 1));
    char * s2 =
        malloc(sizeof(char) * (s2_len + 1));
    str_copy_upper(s_1,s1);
    str_copy_upper(s_2,s2);
    size_t min_len = s1_len < s2_len ?
                     s1_len : s2_len;
    signed int diff;
    size_t i = 0;
    while(i < min_len){
        if((diff = s1[i] - s2[i]) != 0){
            free(s1);
            free(s2);
            return diff;
        } 
        i++;
    }
    free(s1);
    free(s2);
    return s1_len - s2_len;
}

unsigned int compare_file(
    FILE * const f1,
    FILE * const f2
) {
    int c_f1 = 0;
    int c_f2 = 0;
    do{
        c_f1 = fgetc(f1);
        c_f2 = fgetc(f2);
        if(c_f1 != c_f2)
            return 0;
    }while(c_f1 != EOF && c_f2 != EOF );
    return !(fgetc(f1) != EOF ||
             fgetc(f2) != EOF);
}

void read_file (
   char ** buffer,
   size_t * capacity,
   char * path
) {
    FILE * file = fopen(path,"r");
    char * realloc_tmp;
    size_t i = 0;
    if(file != NULL){
        do{
           if(i == *capacity){
                realloc_tmp = realloc(*buffer,*capacity * 2);
                if(realloc_tmp != NULL){
                    *buffer = realloc_tmp;
                    (*capacity) *= 2;
                } else return;
           }
           (*buffer)[i] = fgetc(file);
        }while((*buffer)[i++] != EOF);
        fclose(file);
    }
}
