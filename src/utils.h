/**
 *
 * Declaration of an interface that regroup some useful
 * functions.
 *
 * @author Jordan Gauthier
 * @code permanent GAUJ25089201
 */

#ifndef UTILS_H
#define UTILS_H

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include "genlist.h"

/**
 * Append The content of each node of a Genlist to a string which each of
 * them will also be separated by a comma ','.
 *
 * @param string The initial string
 * @param list The Genlist of char*
 * @param start The starting index (Usualy it should be 0)
 * @return a pointer to the newly allocated string
 */
char * app_genlist_tstr(
    char const * const string,
    Genlist const * const list,
    size_t index
);

/**
 * Transform each characters of a string to its lower case
 * representation.
 *
 * @param str The string to transform
 */
void to_lower_str(
    char * str
);

/**
 * Create a string formed with s1 and s2.
 *
 * @param s1 The first part of the string
 * @param s2 The second part of the string
 * @return A pointer to the newly allocated string
 */
char * create_string(
    char const * const s1,
    char const * const s2
);

/**
 * Copy all the characters in source into target
 * but transform all characters to upper case.
 *
 * @pre source must not be NULL
 * @pre target must not be NULL
 * @pre target must have enough memory case.
 * @param source the source array from which we will copy the characters
 * @param target the target array in which we will copy the characters
 */
void str_copy_upper(
    char const * const source,
    char * const target
);

/**
 * Copy all the characters in source into target
 *
 * @pre source must not be NULL
 * @pre target must not be NULL
 * @pre target must have enough memory case.
 * @param source the source array from which we will copy the characters
 * @param target the target array in which we will copy the characters
 */
void str_copy(
    char const * const source,
    char * const target
);

/**
 * Compare s1 to s2 lexicographicly.
 *
 * @pre s1 must not be NULL
 * @pre s2 must not be NULL
 * @param s1 A pointer to the first char of a string.
 * @param s2 A pointer to the first chaarcter of a string.
 * @return A negative number is s1 is smaller.
 *         A positiv number is s2 is smaller.
 *         0 is both string are equal.
 */
signed int str_compare(
    char const * const s1,
    char const * const s2
);

/**
 * Compare both files together lexicographicly.
 *
 * @pre f1 must not be null
 * @pre f2 must not be null
 * @param f1 A pointer to the first stream
 * @param f2 A pointer to the second stream
 * @return 1 if the files are equal.
 */
unsigned int compare_file(
    FILE * const f1,
    FILE * const f2
);

/**
 * It will first open thefile at the specified path. Then it will
 * copy all the character in the open file to *buffer until
 * the iterate character == EOF. If the *buffer capacity is becoming
 * too small, then realloc is call to increase his capacity. If the
 * realloc function return a NULL pointer the function terminate immediatly.
 * If everything did right, the file previously open is close.
 *
 * @Info : The function do not free anything even if the realloc fail.
 *
 * @param buffer A pointer to a pointer to a string.
 * @param size The initial capacity of *buffer.
 * @param path The path that lead to the file.
 *
 */
void read_file (
    char ** buffer,
    size_t * capacity,
    char * path
);

#endif
