#!/usr/bin/env bats

# Temporary directory for tests

mkdir -p "./tmp"
BATS_TMPDIR="./tmp"

@test "Default program working" {
  run bin/tp3
  [ "$status" -eq 0 ]
}

@test "Error invalid arguments" {
  run bin/tp3 --uhh aih
  [ "$status" -eq 5 ]
}

@test "Error wrong value region" {
  run bin/tp3 --region "south americas"
  [ "$status" -eq 1 ]
  [ "${lines[0]}" = "Error : wrong value." ]
}

@test "Error wrong value format" {
  run bin/tp3 --output-format numeric
  [ "$status" -eq 1 ]
  [ "${lines[0]}" = "Error : wrong value." ]
}

@test "Error too many arguments" {
  run bin/tp3 --output-format png allo
  [ "$status" -eq 2 ]
  [ "${lines[0]}" = "Error : Too many arguments." ]
}

@test "Error mandatory file with png format" {
  run bin/tp3 --output-format png
  [ "$status" -eq 3 ]
  [ "${lines[0]}" = "Error : A file is mandatory while using the png format." ]
}

@test "Error invalid specifications" {
  run bin/tp3 --country zzz
  [ "$status" -eq 7 ]
  [ "${lines[0]}" = "Error : Zero country correspond to the specified specifications." ]
}

@test "Error specifies country and region" {
  run bin/tp3 --country can --region americas
  [ "$status" -eq 8 ]
  [ "${lines[0]}" = "Error : --country and --region can't be specified at the same time." ]
}
